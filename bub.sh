#!/bin/bash

#===============================|
#          WELCOME TO           |
#===============================|
#  _____   __    __   _____     |
# |     \ |  |  |  | |     \    | Big
# | **  / |  |  |  | | **  /    | Ugly
# |    |  |  |  |  | |    |     | Brother
# | **  \ |  \__/  | | **  \    |
# |_____/  \______/  |_____/    |
#===============================|
# to add an application launcher use this as the launch command
#
# pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY /home/$USER/path/to/bub.sh
#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 22/Sep/2018 undergoing development to date.
#------------------------------------------------------------------------------
# BUB is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Or visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch, it was brought to life by  
# an overwhelming number of requests by the arrowlinux group.
#
# For contributions, code, ideas, and other help,
# I give my deepest thanks to: knowledgebase101, erw1030, 
# leon.p (From EzeeTalk), and the arrowlinux group.
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       or
#       paypal.me/eliwal
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
#
#------------------------------------------------------------------------------
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if Zenity is installed. If not, it will install it.
TODAY="$(date)"

if [[ $USER = "root"  ]]
 then
        clear
      if [ -f /usr/bin/dialog ]
           then clear
                clear
                 echo " Dialog is already installed"
                 echo ""
           else apt-get install -y dialog
                clear
                clear
      fi
  echo " ROOT check passed "
  echo ""
  echo " =================================================="
  echo " ========== $TODAY =========="
  else
         dialog --no-lines --msgbox "ROOT check FAILED!\n\nIn a terminal run this with sudo.\n\nlike this     sudo ./bub.sh\nOr this       sudo bash bub.sh" 10 40 #background-color: red
         dialog --no-lines --msgbox "BUB will now close ! ! " 6 30
         clear
         clear
       echo " ROOT check FAILED (must be ran with sudo) like this "
       echo ""
       echo " sudo ./bub.sh"
       echo ""
       echo " =================================================== "
        exit "${?}"
fi
##################################################################################
#=================================================================================
#=================================================================================
# if you have an issue with the variable below, (Line #98) then change it    #====
# Enter your User Name there.                                                #====
#   Like this: USR1='username'   or   USR1="username"                        #====
USR1="$(logname)"                                                            #====
#=================================================================================
#=================================================================================
##################################################################################

DWS1='BUB Workshop'
title='BUB toolbox'
#------------------------------------------------------------------------------
EXT1='EXIT Options'
EXT2='Exit BUB'
EXT3='Just Exit This App'
UPO1='Update Options'
UPA1='Easy Update (Safe)'
UPA2='Easy Update (Full)'
IAD='Install another Desktop'
PPA1='PPA Management'
SFM='Swapfile Management'
CHP1='USERS and Passwords'
GP1='Generate a Password'
LF1='Lotto Fun'
HTU='How to use this Toolbox'
HTU2='How to use this Tool'
BACK_HOME='Back to Home Menu'  
#------------------------------------------------------------------------------
TTLA="${USR1^} Pick An Update Option"
URS1='Update Repository List'
LAUB1='List All Available Updates'
AUS1='Apply Updates (Safe)'
AULS1='Apply Updates (Less Safe)'
UPG1='Update Bootloader (grub)'
#------------------------------------------------------------------------------
kubu1='Install Kubuntu'
lubu1='Install Lubuntu'
xubu1='Install Xubuntu'
gnome1='install Gnome'
MATE1='Install MATE'
OPB1='Install OPENBOX'
#------------------------------------------------------------------------------
LAIP1='List All Installed PPAs'
lypp1='List PPAs You Have Added'
rmpp1='Remove a PPA You Have Added'
install_txt='Choose an IDE or Text Editor'
GRBCMZR='Grub Customizer'
UKUU1='Ubuntu Kernel Upgrade Utility'
ltbp1='Laptop Battery Power'
LMT1='Laptop Mode Tools'
CR1='Google Chrome'
gep1='Google Earth Pro'
UM1='Ubuntu Make'
BRP='Boot Repair'
BB1='Xiphos Bible'
NSI1='Numix Square Icons'
KB1='Kubuntu Backports'
CM1='Conky Manager'
#------------------------------------------------------------------------------
ST2='Sublime text 2'
VSC1='Visual Studio Code (Microsoft)'
BLF1='Bluefish Editor'
CDL='CodeLite Editor'
ATM1='Atom Text Editor'
BACKUP="Back To $PPA1"
#------------------------------------------------------------------------------
MES1='Manually Enter Size'
DS1='Delete Swapfile'
LFS1='Look At fstab, This is Safe'
ASF1='Add swapfile to fstab,  Dangerous! '
#------------------------------------------------------------------------------
USR="Change Password For ${USR1^} "
RT1='Change Password For ROOT'
OTR1='Change Password For Other User'
WH0='List ALL USERS on this Computer'
ANU1='Add a NEW User to the Computer'
DEL1='DELETE / REMOVE User (Danger!)'
CHHN='Change Hostname'
HowTU3='How to use This Tool'
#------------------------------------------------------------------------------
PA1='Power Adminastration'
RB1='Reboot This Computer'
SD1='Shut Down This Computer'
#------------------------------------------------------------------------------
HOM3='Back To Home Menu'
hstnm="$(hostname)"
SSP='Suspend to RAM. Sleep'
HBRNT='Hibernate. Power Off'

while true ; do



# home, update
HOME_HEIGHT=15
HOME_WIDTH=33
HOME_CHOICE_HEIGHT=8

UPDATE_HEIGHT=13
UPDATE_WIDTH=40
UPDATE_CHOICE_HEIGHT=6

IAD_HEIGHT=15
IAD_WIDTH=27
IAD_CHOICE_HEIGHT=9

PPA_HEIGHT=26
PPA_WIDTH=40
PPA_CHOICE_HEIGHT=24

PPA_HEIGHT2=16
PPA_WIDTH2=40
PPA_CHOICE_HEIGHT2=9

SWAP_HEIGHT=23
SWAP_WIDTH=40
SWAP_CHOICE_HEIGHT=23

USER_HEIGHT=18
USER_WIDTH=42
USER_CHOICE_HEIGHT=15

LOTTO_HEIGHT=20
LOTTO_WIDTH=40
LOTTO_CHOICE_HEIGHT=10

HTU_HEIGHT=20
HTU_WIDTH=40
HTU_CHOICE_HEIGHT=10

NEW_HEIGHT=20
NEW_WIDTH=40
NEW_CHOICE_HEIGHT=10

EXIT_HEIGHT=11
EXIT_WIDTH=35
EXIT_CHOICE_HEIGHT=4

HEIGHT=20
HEIGHT2=10
HEIGHT3=22

WIDTH=40
WIDTH2=50

CHOICE_HEIGHT2=12
CHOICE_HEIGHT3=22
CHOICE_HEIGHT4=20
CHOICE_HEIGHT=10
CHOICE_HEIGHT7=3
MENU="Choose an option:"

OPTIONS=(1 "$UPO1"
         2 "$IAD"
         3 "$PPA1"
         4 "$SFM"
         5 "$CHP1"
         6 "$LF1" 
         7 "$HTU" 
         8 "$EXT1")

CHOICE=$(dialog --clear \
                --ok-label "continue" \
                --no-cancel \
                --no-lines \
                --backtitle "$DWS1" \
                --title "$UPO1" \
                --menu "$MENU" \
                $HOME_HEIGHT $HOME_WIDTH $HOME_CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
###############################################################################
########################### Update Management #################################
###############################################################################

        1)
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$LAUB1"
                          2 "$UPA1"
                          3 "$UPA2"
                          4 "$UPG1" 
                          5 "$EXT2"
                          6 "$HOM3")

                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$UPO1" \
                                 --menu "$MENU" \
                                 $UPDATE_HEIGHT $UPDATE_WIDTH $UPDATE_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1)
   #                      LAUPD="bash ~/bin/debupdates2.sh" 
   #                      OUTP1="${LAUPD} > ~/bin/updates-avalible.txt"
                            clear ;  clear ; echo "Getting List..." ; sudo apt-get -qy update > /dev/null
                            NUMOFUPDATES=$(sudo aptitude search "~U" | wc -l)
                            clear ; clear ; apt update ; clear ; clear ; apt list --upgradable
                            echo ""
                            echo "Avalible Updates ${NUMOFUPDATES}"

                            echo ""
                            echo "Press a key to continue"
                            read -rsn1
                            echo "Key pressed" ; sleep 1
                             ;;

                         2) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Safely Update Your System\nWould you like to continue?" 6 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else
                                        NUMOFUPDATES=$(sudo aptitude search "~U" | wc -l)
                                        clear ; clear ; apt update ; clear ; clear ; apt list --upgradable
                                        echo ""
                                        echo "Avalible Updates ${NUMOFUPDATES}"
                                        
                                        echo "" ; sleep 10
                                        apt upgrade -y && clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                fi
                             ;; 

                         3) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System and kernel\nWould you like to continue?" 6 43
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else 
                                       if [ -f /usr/bin/aptitude ]
                                         then clear ; clear
                                              echo "Aptitude is already installed"
                                              echo ""
                                              NUMOFUPDATES=$(sudo aptitude search "~U" | wc -l)
                                              clear ; clear ; apt update ; clear ; clear ; apt list --upgradable
                                              echo ""
                                              echo "Avalible Updates ${NUMOFUPDATES}" 
                                              echo "" ; sleep 10
                                              aptitude -y safe-upgrade
                                              clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                         else 

                                              clear ; clear ; apt update ; clear ; clear ; apt list --upgradable 
                                              echo "" ; sleep 10
                                              apt-get install -y aptitude && aptitude -y safe-upgrade
                                              echo ""
                                              clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                       fi
                                fi
                             ;;

                         4)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update GRUB Usually Safe\nBut Sometimes Not, Would you like to continue?" 7 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else clear ;echo "" ; echo "Updating GRUB Bootloader" ; echo ""
                                        update-grub &&
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "GRUB Update Complete" 6 25
                                fi
                             ;; 

                         5)
                             clear && clear && break 2
                             ;;

                         6)
                             clear && clear && break 1
                             ;;
                 esac
                 done
            ;;


###############################################################################
######################### Install Another Desktop #############################
###############################################################################

        2)
                 while [[ "$?" = "0" ]] ; do
                 clear ; clear
                 OPTIONS=(1 "$kubu1"
                          2 "$lubu1"
                          3 "$xubu1"
                          4 "$gnome1"
                          5 "$MATE1" 
                          6 "$OPB1"
                          7 "$EXT2"
                          8 "$HOM3")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$IAD" \
                                 --menu "$MENU" \
                                 $IAD_HEIGHT $IAD_WIDTH $IAD_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $kubu1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y kubuntu-desktop
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $kubu1 Was successful" 8 30
                                fi
                             ;; 

                         2) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $lubu1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y lubuntu-desktop
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $lubu1 Was successful" 8 30
                                fi
                             ;;


                         3) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $xubu1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y xubuntu-desktop
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $xubu1 Was successful" 8 30
                                fi
                             ;;

                         4) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $gnome1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y ubuntu-gnome-desktop
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $gnome1 Was successful" 8 30
                                fi
                             ;;

                         5)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $MATE1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y mate-desktop mate-desktop-common mate-desktop-environment mate-desktop-environment-core mate-desktop-environment-extra mate-desktop-environment-extras ubuntu-mate-desktop
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $MATE1 Was successful" 8 30
                                fi
                             ;;
 

                         6)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will $OPB1 \nWould you like to continue?" 6 50
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                   else 
                                        clear && apt-get install -y openbox
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        dialog --title "${DWS1}" --no-lines --msgbox "operation $OPB1 Was successful" 8 30
                                fi
                             ;;


                         7)
                             clear && clear && break 2
                             ;;

                         8)
                             clear && clear && break 1
                             ;;


                 esac
                 done

            ;;
            
###############################################################################
################################ PPA Management ###############################
###############################################################################
        3)
#            clear
#            clear
#            dialog --no-lines --msgbox "$PPA1 Will Go Here" 6 0
        
                 clear
                 clear  
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$LAIP1"
                          2 "$lypp1"
                          3 "$rmpp1"
                          4 "$install_txt" 
                          5 "$GRBCMZR"
                          6 "$UKUU1"
                          7 "$ltbp1"
                          8 "$LMT1"
                          9 "$CR1"
                          10 "$gep1"
                          11 "$UM1"
                          12 "$BRP"
                          13 "$NSI1"
                          14 "$KB1"
                          15 "$CM1"
                          16 "$BB1"
                          17 "$HTU2"
                          18 "$EXT2"
                          19 "$BACK_HOME")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$PPA1" \
                                 --menu "$MENU" \
                                 $PPA_HEIGHT $PPA_WIDTH $PPA_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
#----------------------------------------------------------------------
# list ppas
                         1) 
                             clear ; clear
                             ppa2="$(grep -r --include '*.list' '^deb ' /etc/apt/ | sed -re 's/^\/etc\/apt\/sources\.list((\.d\/)?|(:)?)//' -e 's/(.*\.list):/\[\1\] /' -e 's/deb http:\/\/ppa.launchpad.net\/(.*?)\/ubuntu .*/ppa:\1/')" ; echo "$ppa2"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1                    
                             ;; 
#------------------------------------------------------------------------
#list ppas you installed
                         2) 
                             clear ; clear
                             ppa1="$(ls /etc/apt/sources.list.d)" ; echo "$ppa1"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1 
                             ;;
#------------------------------------------------------------------------
#remove ppas
                         3) 
                             clear ; clear
                             mkfifo /tmp/namedPipe17 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "enter the PPA You Want To Remove\nLike This, \ngoogle-earth-pro.list " 8 40 "google.list" 2> /tmp/namedPipe17 & 
                             
                             # release contents of pipe
                                    RMPPA2="$( cat /tmp/namedPipe17  )" 
                                        clear ; clear
                                           if [ -f /etc/apt/sources.list.d/$RMPPA2 ]
                                              then rm /etc/apt/sources.list.d/$RMPPA2
                                                   dialog --title "${DWS1}" --no-lines --msgbox           "/etc/apt/sources.list.d/$RMPPA2 Has Been Removed" 8 50
                                                   clear ; clear
          
                                              else dialog --title "${DWS1}" --no-lines --msgbox           "/etc/apt/sources.list.d/$RMPPA2 Does Not Exist" 8 50
                                                   clear ; clear
                                                   break 1
          
                                           fi
          
                                           if [ -f /etc/apt/sources.list.d/$RMPPA2.save ]
                                              then rm /etc/apt/sources.list.d/$RMPPA2.save
                                           fi
          
                                       echo ""
                                       echo "Press a key to continue"
                                       read -rsn1
                                       echo "Key pressed" ; sleep 1

                             # clean up
                             rm /tmp/namedPipe17 
                             ###########################

                             ;;
#------------------------------------------------------------------------------
# Choose an IDE or Text Editor
                         4) 
                                clear
                                clear  
                                while [[ "$?" = "0" ]] ; do
                                OPTIONS=(1 "$ST2"
                                         2 "$VSC1"
                                         3 "$BLF1"
                                         4 "$CDL"                                        
                                         5 "$ATM1"
                                         6 "$HTU2"
                                         7 "$EXT2"
                                         8 "$BACKUP"
                                         9 "$BACK_HOME")
                                
                                CHOICE=$(dialog --clear \
                                                --ok-label "Continue" \
                                                --cancel-label "Back" \
                                                --no-lines \
                                                --backtitle "$DWS1" \
                                                --title "$PPA1" \
                                                --menu "$MENU" \
                                                $PPA_HEIGHT2 $PPA_WIDTH2 $PPA_CHOICE_HEIGHT2 \
                                                "${OPTIONS[@]}" \
                                                2>&1 >/dev/tty)
                                case $CHOICE in
#-------------------------------------------------------------------
# Sublime Text 2
                                        1) 
                                           clear ; clear
                                           dialog --title "${DWS1}" --no-lines --yesno "$ST2 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                                           if [ "$?" = "0" ]
                                              then clear
                                                   echo "Installing $ST2"
                                                   add-apt-repository -y ppa:webupd8team/sublime-text-2
                                                   apt-get update -y
                                                   apt-get install -y sublime-text
                                                      if [ "$?" = "0" ]
                                                         then 
                                                         dialog --title "${DWS1}" --no-lines --msgbox "$ST2 is installed" 6 40 ;clear ; clear
                                                         else echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $ST2" 6 40 ; clear ; clear
                                                      fi
                                              else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$ST2 has not been installed" 8 40
                                           fi
                                           ;;

#------------------------------------------------------------  
# VS Code
                                        2)
                                            clear ; clear
                                            dialog --title "${DWS1}" --no-lines --yesno "$BLF1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                                           if [ "$?" = "0" ]
                                              then clear
                                                  echo "Installing $VSC1"
                                                  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
                                                  if [ "$?" = "0" ]
                                                     then echo ""
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To curl $VSC1 gpg" 8 40 ; clear ; clear ; break 1
                                                  fi
                                              
                                                  install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
                                                  if [ "$?" = "0" ]
                                                     then echo ""
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To change microsoft.gpg permissions" 8 40 ; clear ; clear ; break 1
                                                  fi
                                                  mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
                                                  if [ "$?" = "0" ]
                                                     then echo ""
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To move microsoft.gpg" 8 40 ; clear ; clear ; break 1
                                                  fi
                                                  sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
                                                  if [ "$?" = "0" ]
                                                     then echo ""
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To sh $VSC1" 8 40 ; clear ; clear ; break 1
                                                  fi
                                                  apt-get install -y apt-transport-https
                                                  if [ "$?" = "0" ]
                                                     then echo ""
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To install apt-transport-https" 8 40 ; clear ; clear ; break 1
                                                  fi
                                                  apt-get update -y
                                                  apt install -y code
                                                  if [ "$?" = "0" ]
                                                     then 
                                                     dialog --title "${DWS1}" --no-lines --msgbox "$VSC1 is installed" 6 40 ;clear ; clear
                                                     else echo ""
                                                     echo "Press a key to continue"
                                                     read -rsn1
                                                     echo "Key pressed" ; sleep 1
                                                     dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $VSC1" 6 40 ; clear ; clear
                                                  fi
                                              else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$VSC1 has not been installed" 8 40
                                            fi
                                            ;;
#-------------------------------------------------------------------
# BlueFish Editor
                                        3)
                                           clear ; clear
                                           dialog --title "${DWS1}" --no-lines --yesno "$BLF1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                                           if [ "$?" = "0" ]
                                              then clear
                                                   echo "Installing $BLF1"
                                                   add-apt-repository -y ppa:klaus-vormweg/bluefish
                                                   apt-get update -y
                                                   apt-get install -y bluefish 
                                                      if [ "$?" = "0" ]
                                                         then 
                                                         dialog --title "${DWS1}" --no-lines --msgbox "$BLF1 is installed" 6 40 ;clear ; clear
                                                         else echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $BLF1" 6 40 ; clear ; clear
                                                      fi
                                              else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$BLF1 has not been installed" 8 40
                                           fi
                                           ;; 

#-----------------------------------------------------------------
# CodeLite Editor
                                        4)
                                           clear ; clear
                                           dialog --title "${DWS1}" --no-lines --yesno "$CDL \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                                           if [ "$?" = "0" ]
                                              then clear ; echo "Installing $CDL"
                                                   add-apt-repository -y ppa:eugenesan/ppa
                                                   apt-get update -y
                                                   apt-get install -y codelite
                                                      if [ "$?" = "0" ]
                                                         then 
                                                         dialog --title "${DWS1}" --no-lines --msgbox "$CDL is installed" 6 40 ;clear ; clear
                                                         else echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $CDL" 6 40 ; clear ; clear
                                                      fi
                                              else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$CDL has not been installed" 8 40
                                           fi
                                           ;; 

#-----------------------------------------------------------------
# Atom Text Editor
                                        5)
                                           clear ; clear
                                           dialog --title "${DWS1}" --no-lines --yesno "$ATM1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                                           if [ "$?" = "0" ]
                                              then clear ; echo "Installing $ATM1"
                                                   add-apt-repository -y ppa:webupd8team/atom
                                                   apt-get update -y
                                                   apt-get install -y atom
                                                      if [ "$?" = "0" ]
                                                         then 
                                                         dialog --title "${DWS1}" --no-lines --msgbox "$ATM1 is installed" 6 40 ;clear ; clear
                                                         else echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $ATM1" 6 40 ; clear ; clear
                                                      fi
                                              else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$ATM1 has not been installed" 8 40
                                           fi
                                           ;;     

#-----------------------------------------------------------------
# How To Use This Tool
                                        6)
                                           clear ; clear
                                           dialog --title "${DWS1}" --no-lines --msgbox "This is the IDE and Text Editor window. Here you chose which Code Editor you want to install.\n\nIn here we have put together the best list of Editors that we could find.\nAt least the ones not already in the repos." 11 50
                                           ;;
#-----------------------------------------------------------------
# QUIT OR EXIT
                                      7) 
                                          clear ; clear                           
                                          break 3
                                          ;;
#-----------------------------------------------------------------
# Go Back Home
                                      8)
                                          clear ; clear
                                          break 1
                                          ;;
#-----------------------------------------------------------------
# Go Back To PPA Management
                                      9)
                                          clear ; clear
                                          break 2
                                          ;;
                                          esac
                                          done
             
                                      ;;
#------------------------------------------------------------------------------
# G
                         5) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$GRBCMZR \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then clear ; echo "Installing $GRBCMZR"
                                     add-apt-repository -y ppa:danielrichter2007/grub-customizer
                                     apt-get update -y
                                     apt-get install -y grub-customizer
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$GRBCMZR is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $GRBCMZR" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$GRBCMZR has not been installed" 8 40
                             fi                            
                             ;;


                         6) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$UKUU1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then clear ; echo "Installing $UKUU1"
                                     add-apt-repository -y ppa:teejee2008/ppa
                                     apt-get update -y
                                     apt-get install -y ukuu
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$UKUU1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $UKUU1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$UKUU1 has not been installed" 8 40
                             fi                            
                             ;;

                         7) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$ltbp1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then clear ; echo "Installing $ltbp1"
                                     add-apt-repository -y ppa:linrunner/tlp
                                     apt-get update -y
                                     apt-get install -y tlp tlp-rdw
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$ltbp1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $ltbp1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$ltbp1 has not been installed" 8 40
                             fi                            
                             ;;
#------------------------------------------------------------------------------
# Laptop Mode Tools
                         8) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$LMT1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then clear ; echo "Installing $LMT1"
                                     add-apt-repository -y ppa:ubuntuhandbook1/apps
                                     apt-get update -y
                                     apt-get install -y laptop-mode-tools && tlp start
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$LMT1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $LMT1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$LMT1 has not been installed" 8 40
                             fi 
                             ;;

#------------------------------------------------------------------------------
# Google Chrome
                         9) 
                             clear ; clear
                             echo "Installing $CR1"
                             touch /etc/apt/sources.list.d/google-chrome.list
                             echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list

                             if [ "$?" = "0" ]
                                 then echo ""
                                 else echo ""
                                 echo "Press a key to continue"
                                 read -rsn1
                                 echo "Key pressed" ; sleep 1
                                 dialog --title "${DWS1}" --no-lines --msgbox " Failed To add $CR1 PPA" 8 40 ; clear ; clear ; break 1
                             fi
                             wget https://dl.google.com/linux/linux_signing_key.pub
                             if [ "$?" = "0" ]
                                 then echo ""
                                 else echo ""
                                 echo "Press a key to continue"
                                 read -rsn1
                                 echo "Key pressed" ; sleep 1
                                 dialog --title "${DWS1}" --no-lines --msgbox " Failed To get signed $CR1 key" 8 50 ; clear ; clear ; break 1
                             fi
                             apt-key add ~/linux_signing_key.pub
                             if [ "$?" = "0" ]
                                 then echo ""
                                 else echo ""
                                 echo "Press a key to continue"
                                 read -rsn1
                                 echo "Key pressed" ; sleep 1
                                 dialog --title "${DWS1}" --no-lines --msgbox " Failed To sign $CR1 key" 8 40 ; clear ; clear ; break 1
                             fi
                             apt-get update -y
                             apt-get install -y google-chrome-stable
                             if [ "$?" = "0" ]
                                then 
                                dialog --title "${DWS1}" --no-lines --msgbox "$CR1 is installed" 6 40 ;clear ; clear
                                else echo ""
                                echo "Press a key to continue"
                                read -rsn1
                                echo "Key pressed" ; sleep 1
                                dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $CR1" 6 40 ; clear ; clear
                             fi 
                             ;;
#---------------------------------------------------------------
# Google Earth Pro
                         10) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$gep1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40 ; clear
                             if [ "$?" = "0" ]
                                then echo "Installing $gep1"
                                     wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
                                        if [ "$?" = "1" ]
                                            then echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $gep1" 6 40 ; clear ; clear ; break 1
                                        fi
                                     sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main" > /etc/apt/sources.list.d/google.list'
                                        if [ "$?" = "1" ]
                                            then echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $gep1" 6 40 ; clear ; clear ; break 1
                                        fi
                                     apt-get update -y && apt-get install -y google-earth-pro-stable
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$gep1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $gep1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$gep1 has not been installed" 8 40
                             fi 
                             ;;
#------------------------------------------------------------------------------
# Ubuntu Make
                         11) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$UM1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then echo "Installing $UM1"
                                     add-apt-repository -y ppa:ubuntu-desktop/ubuntu-make
                                     apt-get update -y
                                     apt-get install -y ubuntu-make
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$UM1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $UM1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$UM1 has not been installed" 8 40
                             fi 
                             ;;

                         12) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$BRP \nWill Now Be Installed\nDo You Want To Continue?" 8 40
                             if [ "$?" = "0" ]
                                then echo "Installing $BRP"
                                     apt-add-repository -y  ppa:yannubuntu/boot-repair
                                     apt-get update -y
                                     apt-get install -y boot-repair
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$BRP is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $BRP" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$BRP has not been installed" 8 40
                             fi 
                             ;;

                         13) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$NSI1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40                       
                             if [ "$?" = "0" ]
                                then clear;clear;echo "Installing $NSI1"
                                     add-apt-repository -y ppa:numix/ppa
                                     apt-get update -y
                                     apt-get install -y numix-icon-theme-square
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$NSI1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $NSI1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$NSI1 has not been installed" 8 40
                             fi 
                             ;;

                         14) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$KB1 \nWill Now Be Installed\nDo You Want To Continue?" 8 40                
                             if [ "$?" = "0" ]
                                then echo "Installing $KB1"
                                     add-apt-repository -y ppa:kubuntu-ppa/backports
                                     apt-get update -y
                                     apt-get install -y kio-gdrive pkg-kde-tools
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$KB1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $KB1" 6 40 ; clear ; clear
                                        fi
                                else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$KB1 has not been installed" 8 40
                             fi 
                             ;;
#--------------------------------------------------------------------
# Conky Manager ...... DOES NOT WORK IN 18.04
                         15) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "$CM1 Does NOT work in Ubuntu 18.04 Do You Want To Continue?" 10 40
                             if [ "$?" = "0" ]
                                   then 
                                        echo "Installing $CM1"
                                        apt-add-repository -y ppa:teejee2008/ppa
                                        apt-get update -y
                                        apt-get install -y conky-manager
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$CM1 is installed" 6 40 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $CM1" 6 40 ; clear ; clear
                                        fi
                                    else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$CM1 has not been installed" 8 40
                             fi 
                             ;;
#---------------------------------------------------------------------------------------
# Bible
                         16) 
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This will install $BB1 \nDo You Want To Continue?" 6 35
                             if [ "$?" = "0" ]
                                   then clear
                                        echo "Installing $BB1"
                                        apt-add-repository -y ppa:unit193/crosswire
                                        apt-get update -y
                                        apt-get install -y  xiphos # sword pysword
                                        if [ "$?" = "0" ]
                                           then 
                                           dialog --title "${DWS1}" --no-lines --msgbox "$BB1 is installed" 6 30 ;clear ; clear
                                           else echo ""
                                           echo "Press a key to continue"
                                           read -rsn1
                                           echo "Key pressed" ; sleep 1
                                           dialog --title "${DWS1}" --no-lines --msgbox " Failed To install $BB1" 6 40 ; clear ; clear
                                        fi
                                    else dialog --title "${DWS1}" --no-lines --msgbox "You Canceled. \n$BB1 has not been installed" 8 40
                             fi 
                             ;;

                         17) 
                             clear ; clear                           
                             dialog --title "${DWS1}" --no-lines --msgbox "This is the IDE and Text Editor window. Here you chose which Code Editor you want to install.\n\nIn here we have put together the best list of Editors that we could find.\nAt least the ones not already in the repos." 11 50
                             ;;

                         18)
                             clear ; clear
                             break 2
                             ;;
                         19)
                             clear ; clear
                             break 1
                             ;;
                 esac
                 done

        ;;

###############################################################################
############################## Swapfile Management ############################
###############################################################################

        4)
        
                 clear
                 clear  
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "choose size"
                          2 "2GB"
                          3 "4GB"
                          4 "6GB" 
                          5 "8GB"
                          6 "10GB"
                          7 "12GB"
                          8 "16GB"
                          9 "24GB"
                          10 "32GB"
                          11 "Delete Swapfile"
                          12 "Look at fstab (Safe)"
                          13 "Let us add swap to fstab"
                          14 "Manually Edit fstab DANGEROUS!"
                          15 "EXIT BUB"
                          16 "$HOM3")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$SFM" \
                                 --menu "$MENU" \
                                 $SWAP_HEIGHT $SWAP_WIDTH $SWAP_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             mkfifo /tmp/namedPipe1 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "This is where you enter the \nsize of swapfile (in GB) you want\nNumbers only, like 8 or 16" 8 40 "4.5" 2> /tmp/namedPipe1 & 

                             # release contents of pipe
                             swF="$( cat /tmp/namedPipe1  )" 
                             clear ; clear
                             swapoff -a && rm -f /swapfile
                             fallocate -l ${swF}G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "${swF}GB Swapfile Has Been Added" 8 30
                             # clean up
                             rm /tmp/namedPipe1 
                             ;; 
#----------------------------------------------------------------------

                         2) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 2G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "2GB Swapfile Has Been Added" 8 30
                             ;;

                         3) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 4G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "4GB Swapfile Has Been Added" 8 30
                             ;;

                         4) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 6G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "6GB Swapfile Has Been Added" 8 30
                             ;;

                         5) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 8G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "8GB Swapfile Has Been Added" 8 30
                             ;;

                         6) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 10G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "10GB Swapfile Has Been Added" 8 30
                             ;;

                         7) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 12G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "12GB Swapfile Has Been Added" 8 30
                             ;;

                         8) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 16G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "16GB Swapfile Has Been Added" 8 30
                             ;;

                         9) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 24G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "24GB Swapfile Has Been Added" 8 30
                             ;;

                         10)
                             clear ; clear
                             swapoff -a
                             fallocate -l 32G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "32GB Swapfile Has Been Added" 8 30
                             ;;

                         11)
                             clear ; clear
                             swapoff -a && rm -f /swapfile
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;

                         12)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --msgbox "At the bottom of the following \nlist, you are looking for \n\n /swapfile none swap defaults 0 0" 10 50
                             echo ""
                             echo " Add this to the bottom of fstab."
                             echo " /swapfile none swap defaults 0 0"
                             echo ""
                             clear ; clear
                             cat /etc/fstab
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;
                         13)
                             echo "/swapfile none swap defaults 0 0" >> /etc/fstab
#                             nano /etc/fstab
                             dialog --title "${DWS1}" --no-lines --msgbox "swapfile was added to fstab. check to make sure it is, Make sure there is only one \ninstance of it.(no duplicates)" 10 50
                             ;;

                         14)
                             dialog --title "${DWS1}" --timeout 5 --no-lines --yesno  "Warning Editing fstab can \nBREAK YOUR SYSTEM! \n Are you sure you want to proceed?" 10 50
                             nano /etc/fstab
                             ;;

                         15)
                             clear ; clear
                             break 2
                             ;;
                         16)
                             clear ; clear
                             break 1
                 esac
                 done

        ;;
# USR="Change Password For ${usnm^} "
# RT1='Change Password For ROOT'
# OTR1='Change Other User Password'
# GP1='Generate a Password'
# WH0='List ALL USERS on this Computer'
# ANU1='Add a NEW User to the Computer'
# DEL1='DELETE / REMOVE User    (Danger!)'
# CHHN='Change Hostname'
# HowTU3='How to use This Tool'   
###############################################################################
############################ USERS AND PASSWORDS ##############################
###############################################################################
        5)

                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$RT1"
                          2 "$USR"
                          3 "$OTR1"
                          4 "$GP1" 
                          5 "$WH0"
                          6 "$ANU1"
                          7 "$DEL1"
                          8 "$CHHN"
                          9 "$HowTU3"
                          10 "EXIT BUB"
                          11 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$CHP1" \
                                 --menu "$MENU" \
                                 $USER_HEIGHT $USER_WIDTH $USER_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
#--------------------------------------------
# Change ROOT password
                         1) 
                             clear ; clear
                             passwd
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ROOT " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ROOT " 8 40
                                fi

                             ;; 
#--------------------------------------------
# Change USER password
                         2) 
                             clear ; clear
                             passwd $USR1
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${USR1^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${USR1^} " 8 40
                                fi
   
                             ;;
#-----------------------------------------------------------------------
# Change other users passwors 
                         3) 
                             clear ; clear
                             mkfifo /tmp/namedPipe2 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "enter new user name" 6 30 "${USR1,,}" 2> /tmp/namedPipe2 & 
                             NUSR1="$( cat /tmp/namedPipe2  )" 
                             usNM="${NUSR1,,}"
                             grep -q "^${usNM}:" /etc/passwd
                             
                             clear ; clear
                             passwd ${usNM}
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${usNM^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${usNM^} " 8 40
                                fi
                             # clean up
                             rm /tmp/namedPipe2 
                             ;; 
#----------------------------------------------------------------------
# Generate Password
                         4) 
                             clear ; clear
                             mkfifo /tmp/namedPipe10 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "6" 2> /tmp/namedPipe10 & 

                             # release contents of pipe
                             GNP1="$( cat /tmp/namedPipe10  )" 
                             clear
                             clear
                             HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "${GNP1}" )"

                             echo ""
                             echo "The Randomly Generated Password Is"
                             echo ""
                             echo "$HASH"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

#                            NPW1='Randomly Generated Password'

#                            HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "$NMB")"
#                             echo -n $HASH # | zenity --text-info --editable --height=10 --width=300 --title="The New $NPW1 Is"

                             dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Password Is \n\n$HASH " 10 40

                             # clean up
                             rm /tmp/namedPipe10
                             ;;

#--------------------------------------------------------------------------------------------------------------
# List ALL USERS on this Computer
                         5) 
                             clear ; clear
                             echo "Here is a List of ALL USERS on this Computer"
                             echo ""
                             LAU1="$(l=$(grep "^UID_MIN" /etc/login.defs) && awk -F':' -v "limit=${l##UID_MIN}" '{ if ( $3 >= limit ) print $1}' /etc/passwd)"
                             echo "$LAU1" 
                             echo ""
                             echo "User \"nobody\" is a pseudo user. And represents "
                             echo "the user with the least permissions on the system."
                             echo "It can be ignored."
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

                             ;;
#----------------------------------------------------------------
# Add New User


 
                         6) 
                             clear ; clear
                             mkfifo /tmp/namedPipe8 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a New USER Name\nFor this Computer." 6 40 2> /tmp/namedPipe8 & 
                             NUSRN1="$( cat /tmp/namedPipe8  )" 
                             usrNM="${NUSRN1,,}"
                             grep -q "^${usrNM}:" /etc/passwd
#                             NIPTA=$(zenity --entry --title="$title" --width=250 --text "Please enter a New USER Name\nFor this Computer." )
#                             nusr="${NIPTA,,}"
#                             grep -q "^${nusr}:" /etc/passwd
                                if [[ "$?" = "0" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${usrNM^^}\" Already Exist! ! \nBUB will now close ! ! " 8 40 ; exit "${?}"
                                fi
                             clear ; clear
                             useradd -m -g users -G adm,lp,audio,video -s /bin/bash $usrNM
                             passwd $usrNM

                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "${usrNM^} Has Been Added" 8 40
                             # clean up
                             rm /tmp/namedPipe8
                             ;;

#-------------------------------------------------------------------
# Remove User
                         7) 
                             clear ; clear
                             mkfifo /tmp/namedPipe3 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter The USER \nYou Want to Delete." 6 40 2> /tmp/namedPipe3 & 
                             NUSRN5="$( cat /tmp/namedPipe3  )" 
                             UsrnM="${NUSRN5,,}"
                             grep -q "^${UsrnM}:" /etc/passwd

                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${UsrnM^^}\" Does NOT Exist! ! \nBUB will now close ! ! " 8 40 ; clear ; clear ; exit "${?}"
                                fi
                             clear
                             clear

                             OPTIONS=(1 "Delete ${UsrnM^} only"
                                      2 "Delete ${UsrnM^} and all files")
                             
                             CHOICE=$(dialog --clear \
                                             --ok-label "Continue" \
                                             --cancel-label "Back" \
                                             --backtitle "$DWS1" \
                                             --title "$title" \
                                             --menu "$MENU" \
                                             10 40 6 \
                                             "${OPTIONS[@]}" \
                                             2>&1 >/dev/tty)
                             case $CHOICE in

                             1)
                               clear && clear && userdel $UsrnM
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Only ${UsrnM^} Has Been Removed \nTheir folders Have NOT" 6 30
                             ;;

                             2)
                               clear && clear && userdel -r -f $UsrnM
                               echo ""
                               echo "Press a key to continue"
                               read -rsn1
                               echo "Key pressed" ; sleep 1
                               dialog --title "${DWS1}" --no-lines --msgbox "${UsrnM^} And ALL Their Folders \nHave Been Removed" 6 30
                             # clean up
                             rm /tmp/namedPipe3
                             ;;
                   esac
                   
                         ;;

#-------------------------------------------------------------------
# Change HostName

                         8) 
                             clear ; clear
                             mkfifo /tmp/namedPipe11   # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a new hostname" 6 40 "$hstnm" 2> /tmp/namedPipe11 & 
                             HOST_NAME="$( cat /tmp/namedPipe11  )" 
                             HsTnM="${HOST_NAME}"

                             echo $HsTnM > /etc/hostname && clear && clear
                             echo "Host Name Has Been Changed To"
                             echo "$HsTnM "
                             echo ""
                             echo "A Reboot Is Required For Changes To Take Affect."
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Host Name Has Been Changed To \n$HsTnM \n\nA Reboot Is Required \nFor Changes To Take Affect." 9 40
                             # clean up
                             rm /tmp/namedPipe11
                             ;;
#-------------------------------------------------------------------
# How To Use
                         9) 
                             clear ; clear
                             echo "$HowTU3 goes here"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "$HowTU3 will go here" 8 30
                             ;;
#-------------------------------------------------------------------
# break loop or go home
                         10)
                             clear ; clear ; break 2
                             ;;

                         11)
                             clear ; clear ; break 1                            
                            ;;
                 esac
                 done

        ;;
            
###############################################################################
################################ Lotto Fun ####################################
###############################################################################
        6)

            clear ; clear
            mkfifo /tmp/namedPipe5 # this creates named pipe, aka fifo
            # to make sure the shell doesn't hang, we run redirection 
            # in background, because fifo waits for output to come out    
            dialog --title "${DWS1}" --cancel-label "Ten Numbers" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "8" 2> /tmp/namedPipe5 & 
            # release contents of pipe
            LTO2="$( cat /tmp/namedPipe5  )" 
            clear
            clear
            LTO="$( seq -w 99 | sort -R | head -"${LTO2}" | fmt | tr " " "-" )"

            echo ""
            echo "The Randomly Generated Lotto Number Is"
            echo ""
            echo "$LTO"
            echo ""
            echo "Press a key to continue"
            read -rsn1
            echo "Key pressed" ; sleep 1

            dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Lotto Number Is \n\n$LTO " 10 45
            # clean up
            rm /tmp/namedPipe5
            echo ""
            ;;

###############################################################################
############################### How To Use ####################################
###############################################################################
        7)
            clear ; clear
            dialog --title "How To Use" --no-lines --msgbox "This is the HOME window of the BUB Toolbox.\nHere you chose which BUB Tool you want to use.\n\n$UPO1: To update your Opperating System.\nThis is the most used Tool.\n\n$IAD: It's as the title implies.\n\n$PPA1: To mount the PPA and install the App(s).\n\n$SFM: To create/delete or ajust Swapfies.\n\n$GP1: Just like the title says, \nit Generates a random password. A very useful Tool.\n\n$LF1: Is a tool  I made for my own amusement.\nIt will generate random lottory numbers.\nJust enter the amount of numbers you want.\n\n$EXT1: Choose between exiting this App, Sleep,\nrebooting, or turning off the Computer." 25 60
            clear ; clear



            ;;

###############################################################################
############################### EXIT Options ##################################
###############################################################################
        8)
            clear ; clear

                 OPTIONS=(1 "$EXT3"
                          2 "$SSP"
                          3 "$RB1"
                          4 "$SD1")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "OK" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$title" \
                                 --menu "$MENU" \
                                 $EXIT_HEIGHT $EXIT_WIDTH $EXIT_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             exit "${?}"
                             ;; 

                         2)
                             clear ; clear
                             systemctl suspend && break 3
                             ;;

                         3) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Reboot YOUR SYSTEM! \nAre you sure you want to Reboot?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then reboot
                                   else echo ""
                                 fi
                             ;;
      

                         4) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Turn Off YOUR SYSTEM! \nAre you sure you want to Power Off \nyour computer?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then shutdown -h now
                                   else echo ""
                                 fi
                             ;;
                 esac
            ;;
###############################################################################
############################### END OF SCRIPT #################################
###############################################################################
esac
done
clear
