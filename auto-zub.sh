#!/bin/bash
exec > >(tee -i /tmp/zub-long.log)

echo ""
echo "##### start of new job #####"
echo ""
# Put the path/script you want it to call here.
ZuB1="/home/eli/Git-Stuff/GitLab/zub/zub.sh"

egrep -q '(envID|VxID):.*[1-9]' /proc/self/status || IONICE=/usr/bin/ionice

# Checks if your on the battery
if which on_ac_power >/dev/null 2>&1; then
    on_ac_power >/dev/null 2>&1
    ON_BATTERY=$?

# Before you ask,,, the answer is "because it works this way".
    [ "$ON_BATTERY" -eq 1 ] && echo "It is not safe to run rsync when on the laptop battery. this script is now closed"&&echo "" && exit 0
fi

# Rebuild the index
if [ -x "$ZuB1" ]
then
	if [ -x "$IONICE" ]
	then
		nice -n 19 $IONICE -c 3 $ZuB1 --quiet
	else
		nice -n 19 $ZuB1 --quiet
	fi
fi
###
#output
echo ""
echo "##### END OF OUTPUT #####"
echo ""


exec 2>&1
###
#Grepit


grep -i --color 'sent \|total size\|connection\|sender] expand\|deleting eli\|total: \|Receiver] expand\|total: \|failed: \|Permission\|rsync error\|error ' /tmp/zub-long.log >> /tmp/zub.log 

echo ""  >> /tmp/zub.log
echo "##################################################################################################"  >> /tmp/zub.log
echo "########################################### END OF OUTPUT ########################################"  >> /tmp/zub.log
echo "##################################################################################################"  >> /tmp/zub.log
echo "" >&2
### END OF SCRIPT ###
#
###############################################
