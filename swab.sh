#!/bin/bash
#
## This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
################################################################
################################################################
#
# this is a script to clean up whatever messy 
# folder you have. Choose the folder you want
# cleaned up, and run it from there It will
# move the files to their respective Folders
# Music, Movies, Pictures and so on.
#---------------------------------------------------------------
# As a normal user it only moves files with the extentions
# listed. So it is reletivly safe. But use caution. 
#---------------------------------------------------------------
################################################################
################################################################
#### DO NOT RUN IN ROOT!!!! This CAN Trash Your File System ####
################################################################
################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       or
#       paypal.me/eliwal
#
################################################################
title='Swab Cleaner'
if [[ $USER = "root"  ]]
 then

         zenity --warning --width=270 --title="$title" --timeout=10 --text="ROOT check FAILED!\n\nYou must NEVER run this with SUDO.\n\nrun like this like this     ./swab.sh\nOr just open a file manager, \nclick on swab.sh and then click RUN "
        zenity --error --width=150 --title="$title" --timeout=2 --text="SWAB will now close ! ! "
         clear
         clear
       echo " ROOT check FAILED (NEVER RUN With Sudo)"
       echo " instead run like this "
       echo ""
       echo " ./swab.sh"
       echo ""
       echo " Or just open a file manager, " 
       echo " click on swab.sh and then click RUN "
       echo " =================================================== "
        exit
  else
 clear
      if [ -f /usr/bin/zenity ]
           then clear
                clear
                 echo " Zenity is already installed"
                 echo ""
           else apt-get install -y zenity
                clear
                clear
      fi

  echo ""
  echo " ROOT check passed. Running as Normal User "
  echo ""
  echo " =================================================== "


fi

#########

#if [[ "$(pwd)" = "/" ]] || [[ "$(pwd)" = "/root" ]]
#   then zenity --error --width=250 --timeout=10 --text="This must NEVER be ran in Root DIRECTORY! \nThis script will now close! ";exit
#        clear
#   else 
#          if [ -f /usr/bin/zenity ]
#             then clear
#                   clear
#                 echo " Zenity is already installed"
#                 echo ""
#             else apt-get install -y zenity
#                  clear
#                 clear
#          fi
#fi


PD1="$(pwd)/"
USR1="$(logname)" 
PD2="/home/$USER"
Zenity_Question()
{
zenity --question --width=500 --text="                                     YOU ARE IN    $PD1 \n\nThis will move ALL the files from The Directory you chose \ninto their respective folders. \n\nsong.mp3 will be moved into /home/$USR1/Music/ \n\nmovie.mp4 will be moved into /home/$USR1/Movies/ \n\nletter.doc will be moved into /home/$USR1/Documents/ \n\nAnd so on, \n\nIf you chose the same Directory that this script is in\nThis script will be moved to     /home/$USER/Script/ \n\nWarning DO NOT run this as ROOT or in the ROOT DIRECTORY! \nThat CAN Trash your system! Otherwise this should be system safe. \n\nIn the next window you will chose the folder you want to clean.\n\nAre you ready to continue?"
if [ "$?" = "1" ]
   then exit
fi
}

What_Directory()
{
    dr1="$(zenity --list --title="$title HOME" --height=370 --width=350 --cancel-label="EXIT" --text="Hello ${USER^} Welcome To SORT\nChoose The Folder You Want Sorted." --radiolist --column "Pick" --column "Opinion" FALSE "$PD1" FALSE "/home/$USER/Public/" FALSE "/home/$USER/Documents/" FALSE "/home/$USER/Script/" FALSE "/home/$USER/Music/" FALSE "/home/$USER/Videos/"  FALSE "/home/$USER/Pictures/" FALSE "/home/$USER/Desktop/" TRUE "/home/$USER/Downloads/" )"

       if [ "$?" = "1" ]
           then exit
       fi

}
#DIR1="$(dr1)"
Music()
{
# if [ -e "$PD1/Music" ];then
if [ -e "$PD2/Music" ];then
       echo -n ""
else
       mkdir $PD2/Music
fi
mv $dr1*.webm "$PD2/Music" 2>/dev/null 
mv $dr1*.vox "$PD2/Music" 2>/dev/null 
mv $dr1*.wma "$PD2/Music" 2>/dev/null 
mv $dr1*.aac "$PD2/Music" 2>/dev/null 
mv $dr1*.ogg "$PD2/Music" 2>/dev/null 
mv $dr1*.m4a "$PD2/Music" 2>/dev/null 
mv $dr1*.raw "$PD2/Music" 2>/dev/null 
mv $dr1*.mp3 "$PD2/Music" 2>/dev/null 
mv $dr1*.wav "$PD2/Music" 2>/dev/null 
mv $dr1*.midi "$PD2/Music" 2>/dev/null 
}

Movies()
{
if [ -e "$PD2/Videos" ];then
       echo -n ""
else
       mkdir $PD2/Videos
fi
mv $dr1*.mkv "$PD2/Videos" 2>/dev/null 
mv $dr1*.flv "$PD2/Videos" 2>/dev/null 
mv $dr1*.qt "$PD2/Videos" 2>/dev/null 
mv $dr1*.mov "$PD2/Videos" 2>/dev/null 
mv $dr1*.mp4 "$PD2/Videos" 2>/dev/null 
mv $dr1*.3gp "$PD2/Videos" 2>/dev/null 
mv $dr1*.avi "$PD2/Videos" 2>/dev/null 
mv $dr1*.mpeg "$PD2/Videos" 2>/dev/null 
mv $dr1*.mpg "$PD2/Videos" 2>/dev/null 
}

Pictures()
{
if [ -e "$PD2/Pictures" ];then
       echo -n ""
else
       mkdir $PD2/Pictures
fi
mv $dr1*.xcf "$PD2/Pictures" 2>/dev/null 
mv $dr1*.bmp "$PD2/Pictures" 2>/dev/null 
mv $dr1*.tif "$PD2/Pictures" 2>/dev/null 
mv $dr1*.jpeg "$PD2/Pictures" 2>/dev/null 
mv $dr1*.jpg "$PD2/Pictures" 2>/dev/null 
mv $dr1*.gif "$PD2/Pictures" 2>/dev/null 
mv $dr1*.png "$PD2/Pictures" 2>/dev/null 
}

Documents()
{
if [ -e "$PD2/Documents" ];then
       echo -n ""
else
       mkdir $PD2/Documents
fi
mv $dr1*.wpd "$PD2/Documents" 2>/dev/null 
mv $dr1*.sxw "$PD2/Documents" 2>/dev/null 
mv $dr1*.fb2 "$PD2/Documents" 2>/dev/null 
mv $dr1*.htm "$PD2/Documents" 2>/dev/null 
mv $dr1*.html "$PD2/Documents" 2>/dev/null 
mv $dr1*.ps "$PD2/Documents" 2>/dev/null 
mv $dr1*.rtf "$PD2/Documents" 2>/dev/null 
mv $dr1*.doc "$PD2/Documents" 2>/dev/null 
mv $dr1*.pdf "$PD2/Documents" 2>/dev/null 
mv $dr1*.docx "$PD2/Documents" 2>/dev/null 
mv $dr1*.bak "$PD2/Documents" 2>/dev/null 
mv $dr1*.md "$PD2/Documents" 2>/dev/null 
mv $dr1*.odt "$PD2/Documents" 2>/dev/null 
mv $dr1*.txt "$PD2/Documents" 2>/dev/null 
}

Script()
{
if [ -e "$PD2/Scripts" ];then
       echo -n ""
else
       mkdir $PD2/Scripts
fi
#######################################
### Uncomment what you want moved.  ###
### but USE CAUTION! Some of these  ###
###  are used buy your system.      ###
#######################################
mv $dr1*.sh "$PD2/Scripts" 2>/dev/null 
#mv $dr1*.py "$PD2/Script" 2>/dev/null
#mv $dr1*.html "$PD2/Script" 2>/dev/null
#mv $dr1*.xml "$PD2/Script" 2>/dev/null
#mv $dr1*.js "$PD2/Script" 2>/dev/null
#mv $dr1*.vbs "$PD2/Script" 2>/dev/null
#mv $dr1*.scpt "$PD2/Script" 2>/dev/null
#mv $dr1*.php "$PD2/Script" 2>/dev/null
#mv $dr1*.asp "$PD2/Script" 2>/dev/null

}

Public()
{
if [ -e "$PD2/Public" ];then
       echo -n ""
else
       mkdir $PD2/Public
fi
mv $dr1*.iso "$PD2/Public" 2>/dev/null 
mv $dr1*.exe "$PD2/Public" 2>/dev/null 

}

Compressed()
{
if [ -e "$PD2/Compressed" ];then
       echo -n ""
else
       mkdir $PD2/Compressed
fi
mv $dr1*.bat "$PD2/Compressed" 2>/dev/null 
mv $dr1*.deb "$PD2/Compressed" 2>/dev/null 
mv $dr1*.zip "$PD2/Compressed" 2>/dev/null 
mv $dr1*.rar "$PD2/Compressed" 2>/dev/null 
mv $dr1*.7z "$PD2/Compressed" 2>/dev/null 
mv $dr1*.tar "$PD2/Compressed" 2>/dev/null 
mv $dr1*.tar.bz2 "$PD2/Compressed" 2>/dev/null 
mv $dr1*.tar.gz "$PD2/Compressed" 2>/dev/null 
}

################################################
# Order Of Execution
Zenity_Question
What_Directory
Music
Movies
Pictures
Documents
Script
Public
Compressed

# this only moves the file formats listed
