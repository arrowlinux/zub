# swab

# This is a simple zenity script I wrote to clean up a selected folder
# It is usually safe to use as long as it is not ran in root.
# I built it to be as safe as posible yet still get the job done.
# I use this script often. It is in my top ten favorites.

# to use, give permissions
chmod +x swab.sh
# open like this
./swab.sh 
# or just open a file manager and click on it and choose RUN

#-------------------------------------------------------------------

# zub

# #1 in my top ten favorites is without a doubt ZUB!

# I wrote this zenity script to install various things
# and to manage Ubuntu, and Linux Mint updates. And along the way I added many
# other things. including a few Apps, most that are not in the repos.
# And it can be done with piont and click ease. 

# to use give permissions
chmod +x zub.sh
# In a terminal, open as root like this
sudo ./zub.sh

# Running this script will not change anything on your computer untill you tell it to.
# The only thing it does without asking is install zenity (if not already installed).
# And you can back out or exit at almost any point. anytime there is a choice that
# can potentially do harm, you are advised. And usually given extra chances to backout.
# Don't be afraid to explore this script. Just pay attention to what you click on.

# What can ZUB do for you?:

# One click updates. Choose between "safe" 
# and "less safe" ( Includes kernel updates )

# List All Available Updates

# Update Bootloader (grub) useful when dual booting

# Install another DeskTop Environment
# Kubuntu, Lubuntu, Xubuntu, MATE, Or OPENBOX

# List All Installed PPAs

# List PPAs You Have Added

# Remove a PPA You Have Added

# Add Ubuntu Kernel Upgrade Utility

# Add Laptop Battery Power App

# Add Laptop Mode Tools

# Add Google Chrome

# Add Google Earth Pro

# Add Sublime text 2

# Add Visual Studio Code (Microsoft)

# Add Atom Text Editor

# Add Numix Square Icons

# Add Brightbox Ruby

# Add Kubuntu backports

# Add Kubuntu Backports

# Add Conky Manager
Conky Manager PPA Does NOT work in 18.04 or Mint 19

# Swapfile Management
# Add, Remove, Resize a swapfile.
# also you can Add swapfile to fstab.
Editing the swapfile is usualy harmless. However,
Editing fstab can break your system if done wrong.
We take every messure to make sure that does not happen.
And even make a backup of the fstab BEFORE any changes are made.
giving you the ability to recover should you make a mistake.

# USERS and Passwords
# here you can chang Root or any USER password in the system
# You can generate a random Password
# You can add or remove (Delete) a User
WARNING! There is no way to recover a Deleted User! Be Carefull!
# You Can change the Hostname
A reboot is required for changes to take affect.

# Lotto Fun
# You can generate a random lottory number
# A fun little tool

# And the list is growing nearly every day

# Be sure to check back often for the latest updates.

# Please help me welcome my newest addition, BUB (Big Ugly Brother). it is built with dialog to run in a console or terminal. it does everything that its little brother does, only a little bigger and not as handsome. its target is servers running ubuntu. or variants of it.

# BUB also must be ran with sudo like this
chmod +x bub.sh
sudo ./bub.sh
# BUB is being built at the time of this wrighting. and should be ready for public use by October 15 but could be longer.