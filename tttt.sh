#!/bin/bash
USR1="$(logname)"
HOM3='Back To Home Menu'
DWS1='BOW Workshop'
MENU="Choose an option:"
MENU9="Choose an AUR Helper/Manager"
DWS2='BOW Workshop. Here you need to install an AUR Helper/Manager'
AUR1="AUR Helper Not Installed"

AURHLP_HEIGHT=12
AURHLP_WIDTH=35
AURHLP_CHOICE_HEIGHT=8

AUR_HEIGHT=12
AUR_WIDTH=32
AUR_CHOICE_HEIGHT=8



         #        clear
         #        clear  
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "Add An AUR Manager"
                          2 "Remove An AUR Manager"
                          3 "List Installed AUR Manager(s)"
                          4 "EXIT BOW"
                          5 "$HOM3")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$SFM" \
                                 --menu "$MENU" \
                                 $AURHLP_HEIGHT $AURHLP_WIDTH $AURHLP_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in

                          1)
                                         OPTIONS=(
                                                  1 "Install yay"
                                                  2 "Install pakku"
                                                  3 "Install trizen" 
                                                  4 "Install Yaourt"
                                                  5 "Exit Bow")
                        
                                         CHOICE=$(dialog --clear \
                                                         --ok-label "Continue" \
                                                         --cancel-label "Back" \
                                                         --no-lines \
                                                         --backtitle "$DWS2" \
                                                         --title "$AUR1" \
                                                         --menu "$MENU9" \
                                                         $AUR_HEIGHT $AUR_WIDTH $AUR_CHOICE_HEIGHT \
                                                         "${OPTIONS[@]}" \
                                                         2>&1 >/dev/tty)
                                         case $CHOICE in
                      
# Install yay

                                                1)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/yay.git
                                                  cd yay
                                                  sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -dR yay/
                                                  touch /srv/AURhlpr.txt && echo "yay" > /srv/AURhlpr.txt
                                                  ;;
# Install pakku
                                                2)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/pakku.git
                                                  cd pakku
                                                  sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -dR /home/$USR1/pakku/                                                    
                                                  touch /srv/AURhlpr.txt && echo "pakku" > /srv/AURhlpr.txt
                                                  ;;
# Install trizen
                                       
                                                3) 
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/trizen.git
                                                  cd trizen
                                                  sudo -u $USR1 makepkg -si #--noconfirm
                                                  cd ..
                                                  rm -dR /home/$USR1/trizen/ 
                                                  touch /srv/AURhlpr.txt && echo "trizen" > /srv/AURhlpr.txt
                                                  ;;
# Install yaourt

                                                4)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/"${USR1}"
                                                  pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/package-query.git
                                                  cd /home/$USR1/package-query/ && sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/yaourt.git
                                                  cd /home/$USR1/yaourt/ && sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -dR /home/$USR/yaourt/ package-query/
                                                  touch /srv/AURhlpr.txt && echo "yaourt" > /srv/AURhlpr.txt
                                                  ;;
                                          esac
                            ;;

                          2)
                            clear ; clear
                            clear ; clear

                                         OPTIONS=(
                                                  1 "Remove yay"
                                                  2 "Remove pakku"
                                                  3 "Remove trizen" 
                                                  4 "Remove Yaourt"
                                                  5 "Remove ALL"
                                                  6 "Exit Bow"
                                                  7 "$HOM3")
                        
                                         CHOICE=$(dialog --clear \
                                                         --ok-label "Continue" \
                                                         --cancel-label "Back" \
                                                         --no-lines \
                                                         --backtitle "$DWS2" \
                                                         --title "$AUR1" \
                                                         --menu "$MENU9" \
                                                         $AUR_HEIGHT $AUR_WIDTH $AUR_CHOICE_HEIGHT \
                                                         "${OPTIONS[@]}" \
                                                         2>&1 >/dev/tty)
                                         case $CHOICE in
                                         	1) clear ; clear
                                              if [ -f /usr/bin/yaourt ]
                                                 then
                                                 sudo pacman -Rs yaourt
                                              fi
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            2) clear ; clear
                                              if [ -f /usr/bin/yay ]
                                                 then
                                                 sudo pacman -Rs yay
                                              fi
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            3) clear ; clear
                                              if [ -f /usr/bin/pakku ]
                                                 then
                                                 sudo pacman -Rs pakku
                                              fi
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;
                                            
                                            4) clear ; clear
                                              if [ -f /usr/bin/trizen ]
                                                 then
                                                 sudo pacman -Rs trizen
                                              fi
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            5) clear ; clear
                                              if [ -f /usr/bin/yaourt ]
                                                 then
                                                 sudo pacman -Rs yaourt
                                              fi

                                              if [ -f /usr/bin/yay ]
                                                 then
                                                 sudo pacman -Rs yay
                                              fi

                                              if [ -f /usr/bin/pakku ]
                                                 then
                                                 sudo pacman -Rs pakku
                                              fi

                                              if [ -f /usr/bin/trizen ]
                                                 then
                                                 sudo pacman -Rs trizen
                                              fi

                                              echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              ;;

                                            6) clear ; clear ; exit
                                              ;;

                                            7) clear ; clear ; break 1
                                              ;;  
                                        esac
                            ;;

                          3)
                            clear ; clear
                            if [ -f /usr/bin/yaourt ]
                                            then
                                                echo "yaourt is installed"
                                                echo ""
                                          #      echo "yaourt" > /srv/AURhlpr.txt
                                         fi

                                         if [ -f /usr/bin/yay ]
                                            then
                                                echo "yay is installed"
                                                echo ""
                                          #      echo "yay" > /srv/AURhlpr.txt
                                         fi

                                         if [ -f /usr/bin/pakku ]
                                            then
                                                echo "pakku is installed"
                                                echo ""
                                           #     echo "pakku" > /srv/AURhlpr.txt
                                         fi

                                         if [ -f /usr/bin/trizen ]
                                            then
                                                echo "trizen is installed"
                                                echo ""
                                           #     echo "trizen" > /srv/AURhlpr.txt
                                         fi
                            echo "End of list"
                            echo ""
                            echo "Press a key to continue"
                            read -rsn1
                            echo "Key pressed" ; sleep 1

                            ;;

                          4)
                            clear ; clear ; break 2
                            ;;

                          5)
                            clear ; clear ; break 1
                            ;;
                 esac
                 done           

                         # ;;

      #           done
