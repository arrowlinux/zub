#!/bin/bash

#===============================|
#          WELCOME TO           |
#===============================|
#  ______  __    __   _____     |
# |___  / |  |  |  | |     \    |
#    / /  |  |  |  | | **  /    |
#   / /   |  |  |  | |    |     |
#  / /__  |  \__/  | | **  \    |
# /_____|  \______/  |_____/    |
#===============================|
# mkdir {2018..2024}-{1..12}
#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 14/Aug/2018 undergoing development to date.
#------------------------------------------------------------------------------
# ZUB is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# Although I wrote this script from scratch, it was inspired (in part)
# by the Arch ZEN Installer. An excellent piece of work.
# Written by Jody James.
#
# For contributions, code, ideas, and other help,
# I give my deepest thanks to: knowledgebase101, erw1030, and arrowlinux.
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       paypal.me/eliwal
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
#
#------------------------------------------------------------------------------
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if Zenity is installed. If not, it will install it.
TODAY="$(date)"

if [[ $USER = "root"  ]]
 then
        clear
      if [ -f /usr/bin/zenity ]
           then clear
                clear
                 echo " Zenity is already installed"
                 echo ""
           else apt-get install -y zenity
                clear
                clear
      fi
  echo " ROOT check passed "
  echo ""
  echo " =================================================="
  echo " ========== $TODAY =========="
  else
         zenity --warning --width=270 --title="$title" --timeout=10 --text="ROOT check FAILED!\n\nIn a terminal run this with sudo.\n\nlike this     sudo ./zub.sh\nOr this       sudo bash zub.sh"
        zenity --error --width=150 --title="$title" --timeout=2 --text="ZUB will now close ! ! "
         clear
         clear
       echo " ROOT check FAILED (must be ran with sudo) like this "
       echo ""
       echo " sudo ./zub.sh"
       echo ""
       echo " =================================================== "
        exit "${?}"
fi
###############################################################################
#==============================================================================
#==============================================================================
# if you have an issue with the variable below, Line #94 then change it.  #====
# Enter your User Name there.                                             #====
#   Like this: USR1='username'   or   USR1="username"                     #====
USR1="$(logname)"                                                         #====
#==============================================================================
#==============================================================================
###############################################################################
#start_window
CHm2=$(zenity --entry --title="$title" --entry-text "$USR1" --text "Please enter YOUR USER NAME\nFor this Computer." )
usnm="${CHm2,,}"
grep -q "^${usnm}:" /etc/passwd
            if [[ "$?" = "1" ]]
               then zenity --error --width=250 --title="$title" --timeout=10 --text="ERROR User \"${usnm^^}\" Does Not Exist! ! \nZUB will now close ! ! " ; start_window

            fi


#------------------------------------------------------------------------------
while true ; do
      
#------------------------------------------------------------------------------
# HST1='hostname-here'
# EML1='your@email.com'
title='ZUB toolbox'
EXT1='EXIT Options'
EXT2='Exit This App'
EXT3='Just Exit This App'
UPO1='Update Options'
UPA1='Easy Update (Safe)'
UPA2='Easy Update (Full)'
IAD='Install another Desktop'
PPA1='PPA Management'
SFM='Swapfile Management'
CHP1='USERS and Passwords'
GP1='Generate a Password'
LF1='Lotto Fun'
HTU='How to use this Toolbox'
HTU2='How to use this Tool'
BACK_HOME='Back to Home Menu'     
#------------------------------------------------------------------------------
# Home Window

home="$(zenity --list --title="$title HOME" --height=350 --width=250 --cancel-label="Reset App" --text="Hello ${usnm^} Welcome To ZUB\nChoose The Tool You Need." --radiolist --column "Pick" --column "Opinion" FALSE "$UPO1" FALSE "$IAD" FALSE "$PPA1" FALSE "$SFM" FALSE "$CHP1"  FALSE "$LF1" FALSE "$HTU" TRUE "$EXT1" )"


      while [[ "$home" = "$UPO1" ]] ; do
      #------------------------------------------------------------------------------
      TTLA="${usnm^} Pick An Update Option"
      URS1='Update Repository List'
      LAUB1='List All Available Updates'
      AUS1='Apply Updates (Safe)'
      AULS1='Apply Updates (Less Safe)'
      UPG1='Update Bootloader (grub)'
      
#------------------------------------------------------------------------------
# Update Options
          if [[ "$home" = "$UPO1" ]]
              then UP0="$(zenity  --list --title="$title" --height=350 --width=250 --cancel-label="Back" --text "$TTLA" --radiolist  --column "Pick" --column "Opinion" FALSE "$UPA1" FALSE "$UPA2" FALSE "$LAUB1" FALSE "$AUS1" FALSE "$AULS1" FALSE "$UPG1" FALSE "$HTU2" FALSE "$EXT2" TRUE "$BACK_HOME" )"
                           #FALSE "$URS1"
      
#------------------------------------------------------------------------------
# Easy Update (Safe)
              if [[ "$UP0" = "$UPA1" ]]
                 then (
                       echo "0"
                       echo "# Updating The Repository List"
                       apt-get update -y
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title";
                       LSTA="$(apt list --upgradable)" ; echo "$LSTA" | zenity --text-info --width=900 --height=600 --timeout=15 --title="$title"
      
                       (
                       echo "0"
                       echo "# Installing The Updates"
                       apt-get upgrade -y
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                       zenity --info --width=150 --title="$title" --text="Update is Completed"
      
              fi
      
#------------------------------------------------------------------------------
# Easy Update (Full)
              if [[ "$UP0" = "$UPA2" ]]
                   then  ( echo "0"
                       echo "# Updating The Repository List"
                       apt-get update -y
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
      
      
                            if [ -f /usr/bin/aptitude ]
                               then clear
                                    clear
                                    echo "Aptitude is already installed"
                                    echo ""
                               else (
                                   echo "0"
                                   echo "# Installing The Necessary Dependencies"
# Note To Self.... Find Another Way to Do Install Kernel Updates Without Using Aptitude
                                   apt-get install -y aptitude
                                   echo "100"
                                  ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                            fi
                            LSTA="$(apt list --upgradable)"
                            echo "$LSTA" | zenity --text-info --width=900 --height=600 --timeout=10 --title="$title";
                                 if  [ "$?" = "1" ]
                                     then exit "${?}"
                                 fi
      
                            (
                             echo "0"
                             echo "# Installing The Updates"
# Note To Self.... Find Another Way to Do Install Kernel Updates Without Using Aptitude
                             aptitude -y safe-upgrade
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                             zenity --info --width=150 --title="$title" --text="Update is Completed.\nYou might need to Reboot."
              fi
      
#------------------------------------------------------------------------------
# List All Available Updates
              if [[ "$UP0" = "$LAUB1" ]]
                 then(
                       echo "0"
                       echo "# Updating The Repository List"
                       apt-get update -y
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                        LSTA="$(apt list --upgradable)"
                   echo "$LSTA" | zenity --text-info --width=900 --height=600 --title="$title"
              fi
      
              if [[ "$UP0" = "$AUS1" ]]
                 then (
                       echo "0"
                       echo "# Installing The Updates"
                       apt-get upgrade -y
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                   zenity --info --width=150 --title="$title" --text="Update is Completed"
              fi
      
                      if [[ "$UP0" = "$AULS1" ]]
                        then
# Note To Self.... Find Another Way to Do Install Kernel Updates Without Using Aptitude
                            if [ -f /usr/bin/aptitude ]
                               then clear
                                    clear
                                    echo "Aptitude is already installed"
                                    echo ""
                               else (
                                   echo "0"
                                   echo "# Installing The Necessary Dependencies"
# Note To Self.... Find Another Way to Do Install Kernel Updates Without Using Aptitude
                                   apt-get install -y aptitude
                                   echo "100"
                                  ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                            fi
                            (
                             echo "0"
                             echo "# Installing The Updates"
# Note To Self.... Find Another Way to Do Install Kernel Updates Without Using Aptitude
                             aptitude -y safe-upgrade
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                             zenity --info --width=150 --title="$title" --text="Update is Completed.\nYou might need to Reboot."
              fi
      
              if [[ "$UP0" = "$EXT2" ]]
                 then exit "${?}"
              fi
      
              if [[ "$UP0" = "$UPG1" ]]
                 then (
                       echo "0"
                       echo "# Updating GRUB Bootloader"
                       update-grub
                       echo "100"
                       ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                      zenity --info --width=250 --title="$title" --text="The GRUB Boot Loader Is Up To Date"
              fi
              
              if [[ "$UP0" = "$BACK_HOME" ]]
                 then break 1
              fi
              
              if [[ "$UP0" = "$HTU2" ]]
                 then zenity --info --title="$title" --width=650 --text "This is the $UPO1 Tool.\n\nHere you can Use Easy update, To do it All. \nSafe does not install kernel updates. \nFull, installs everthing. \n\nThere are two (2) Update Options, $AUS1 and (Less safe)\n\nThe Safe option updates everything except for the kernel and some core system fies.\nThis is the one you normally want to use.\n\nThe Less Safe option installs EVERYTHING! \nOnly use this if you need to update the kernel, or other core parts of the system. \n\nList Available Updates. shows you upgradable Apps.\nIf it ONLY says ...Listing... then there are no updates Available. \n\n$UPG1: \nThis tool updates your GRUB Bootloader,\nThis is useful when you have multiple Operating Systems.\nor when you have changed your kernel settup. There is a slight risk to this. But it is usually safe to run.\nThough, if you are not familiar with GRUB\n\nUSE AT YOUR OWN RISK\nThe controle (and responsibility) is in your hands."
              fi
      
          fi
#}
      done
      # #------------------------------------------------------------------------------
      while [[ "$home" = "$IAD" ]] ; do
      #------------------------------------------------------------------------------
      kubu1='Install Kubuntu'
      lubu1='Install Lubuntu'
      xubu1='Install Xubuntu'
      mate1='Install MATE'
      OPB1='Install OPENBOX'
      
      
      #------------------------------------------------------------------------------
      # install desktop
              if [[ "$home" = "$IAD" ]]
             then DSK="$(zenity  --list --title="$title" --text="${usnm^} What do you want to install?" --cancel-label="Back" --height=320 --width=250  --radiolist  --column "Pick" --column "Opinion"  FALSE "$kubu1" FALSE "$xubu1" FALSE "$mate1" FALSE "$lubu1" FALSE "$OPB1" FALSE "$HTU2" FALSE "$EXT2" TRUE "$BACK_HOME" )"
      
              if [[ "$DSK" = "$kubu1" ]]
                 then
                zenity --question --title="$title" --width=250 --text "This will $kubu1 Desktop.\nDo You Wish To Continue?"
      
                  if [ "$?" = "0" ]
                     then (
                          echo "0"
                          echo "# Installing Kubuntu Desktop"
                          apt-get install -y kubuntu-desktop
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                          zenity --info --title="$title" --width=200 --text "KDE Plasma is installed"
                  fi
              fi
      
              if [[ "$DSK" = "$xubu1" ]]
                 then
                zenity --question --title="$title" --width=250 --text "This will $xubu1 Desktop.\nDo You Wish To Continue?"
      
                  if [ "$?" = "0" ]
                     then (
                          echo "0"
                          echo "# Installing Xubuntu Desktop"
                          apt-get install -y xubuntu-desktop
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                         zenity --info --title="$title" --width=200 --text "Xubuntu is installed"
                  fi
              fi
      
              if [[ "$DSK" = "$OPB1" ]]
                 then
                zenity --question --title="$title" --width=250 --text "This will $OPB1.\nDo You Wish To Continue?"
      
                  if [ "$?" = "0" ]
                     then (
                          echo "0"
                          echo "# Installing OpenBox"
                          apt-get install -y openbox
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                         zenity --info --title="$title" --width=200 --text "OPENBOX is installed"
                  fi
              fi
      
              if [[ "$DSK" = "$mate1" ]]
                 then
                zenity --question --title="$title" --width=250 --text "This will $mate1 Desktop.\nDo You Wish To Continue?"
      
                  if [ "$?" = "0" ]
                     then (
                          echo "0"
                          echo "# Installing MATE Desktop"
                          apt-get install -y mate-desktop mate-desktop-common mate-desktop-environment mate-desktop-environment-core mate-desktop-environment-extra mate-desktop-environment-extras ubuntu-mate-desktop
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                         zenity --info --title="$title" --width=200 --text "Xubuntu is installed"
                  fi
              fi
      
              if [[ "$DSK" = "$lubu1" ]]
                 then
                zenity --question --title="$title" --width=250 --text "This will $lubu1 Desktop.\nDo You Wish To Continue?"
      
                if [ "$?" = "0" ]
                  then (
                          echo "0"
                          echo "# Installing Lubuntu Desktop"
                          apt-get install -y lubuntu-desktop
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                      zenity --info --title="$title" --width=200 --text "Lubuntu is installed"
                fi
              fi
              if [[ "$DSK" = "$HTU2" ]]
                 then
                      zenity --info --title="$title" --width=450 --text "$IAD:\n\nThis is where you can $IAD.\n\nJust click the one you want, and ZUB will install everthing\n\nneeded. You will be able to load another Desktop Environment.\n\nThe DE of your choice."
              fi
      
              if [[ "$DSK" = "$EXT2" ]]
                  then exit "${?}"
              fi
      
              if [[ "$DSK" = "$BACK_HOME" ]]
                 then break 1
              fi
          fi
       done
      #------------------------------------------------------------------------------
       while [[ "$home" = "$PPA1" ]] ; do
      #------------------------------------------------------------------------------
      LAIP1='List All Installed PPAs'
      lypp1='List PPAs You Have Added'
      rmpp1='Remove a PPA You Have Added'
      install_txt='Choose an IDE or Text Editor'
      GRBCMZR='Grub Customizer'
      UKUU1='Ubuntu Kernel Upgrade Utility'
      ltbp1='Laptop Battery Power'
      LMT1='Laptop Mode Tools'
      CR1='Google Chrome'
      gep1='Google Earth Pro'
      UM1='Ubuntu Make'
      BRP='Boot Repair'
      NSI1='Numix Square Icons'
      KB1='Kubuntu Backports'
      CM1='Conky Manager'
      
      #------------------------------------------------------------------------------
      # PPA'S
          if [[ "$home" = "$PPA1" ]]
              then pps="$(zenity --list --title="$title" --height=570 --width=270 --cancel-label="Back" --text "${usnm^} Pick a PPA to Add and install" --radiolist --column "Pick" --column "Opinion" FALSE "$LAIP1" FALSE "$lypp1" FALSE "$rmpp1" FALSE "$install_txt" FALSE "$GRBCMZR" FALSE "$UKUU1"  FALSE "$ltbp1" FALSE "$LMT1" FALSE "$CR1" FALSE "$gep1" FALSE "$UM1" FALSE "$BRP" FALSE "$NSI1" FALSE "$KB1" FALSE "$CM1" FALSE "$HTU2" FALSE "$EXT2" TRUE "$BACK_HOME" )"
      
      
             if [[ "$pps" = "$LAIP1" ]]
                then ppa2="$(grep -r --include '*.list' '^deb ' /etc/apt/ | sed -re 's/^\/etc\/apt\/sources\.list((\.d\/)?|(:)?)//' -e 's/(.*\.list):/\[\1\] /' -e 's/deb http:\/\/ppa.launchpad.net\/(.*?)\/ubuntu .*/ppa:\1/')" ; echo "$ppa2" | zenity --text-info --cancel-label="Back" --width=700 --height=700 --title="$title"
             fi
      
             if [[ "$pps" = "$lypp1" ]]
                then ppa1="$(ls /etc/apt/sources.list.d)"
                     echo "$ppa1" | zenity --text-info --cancel-label="Back" --width=500 --height=700 --title="$title"
             fi
      
             if [[ "$pps" = "$rmpp1" ]]
               then RMppa="$(zenity --entry --width=250 --title="$title" --text="enter the PPA You Want To Remove\nLike This, \ngoogle-earth-pro.list " --entry-text "google-chrome.list")"
                    if [ -f /etc/apt/sources.list.d/$RMppa ]
                       then rm /etc/apt/sources.list.d/$RMppa
                            zenity --info --title="$title" --width=450 --text "/etc/apt/sources.list.d/$RMppa Has Been Removed"
                       else zenity --info --title="$title" --width=450 --text "/etc/apt/sources.list.d/$RMppa Does Not Exist"
                    fi
      
                    if [ -f /etc/apt/sources.list.d/$RMppa.save ]
                       then rm /etc/apt/sources.list.d/$RMppa.save
                    fi
             fi
      
#------------------------------------------------------------------------------
             while [[ "$pps" = "$install_txt" ]] ; do
#------------------------------------------------------------------------------
            ST2='Sublime text 2'
            VSC1='Visual Studio Code (Microsoft)'
            BLF1='Bluefish Editor'
            CDL='CodeLite Editor'
            ATM1='Atom Text Editor'
            BACKUP="Back To $PPA1"
#------------------------------------------------------------------------------
# sub window for text editors and IDE's
                   if [[ "$pps" = "$install_txt" ]]                 
                      then instppa="$(zenity --list --title="$title" --height=350 --width=270 --cancel-label="Back" --text "${usnm^} Pick a PPA to Add and install" --radiolist --column "Pick" --column "Opinion" FALSE "$ST2" FALSE "$VSC1" FALSE "$BLF1" FALSE "$CDL" FALSE "$ATM1" FALSE "$HTU2" FALSE "$EXT2" FALSE "$BACKUP" TRUE "$BACK_HOME" )"
        
# Sublime Text 2
                          if [[ "$instppa" = "$ST2" ]]
                             then
                                zenity --question --title="$title" --width=250 --text "This will mount the $ST2 PPA\nAnd install the App.\nAre you ready?"
                                if [ "$?" = "0" ]
                                   then (
                                           echo "25"
                                           echo "# Adding Repository"
                                           add-apt-repository -y ppa:webupd8team/sublime-text-2
                                           echo "50"
                                           echo "# Updating Repo List"
                                           apt-get update -y
                                           echo "75"
                                           echo "# Installing $ST2"
                                           apt-get install -y sublime-text
                                           echo "99" ; sleep 1
                                           echo "100"
                                           ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="$ST2 Installer"
                                           zenity --info --title="$title" --width=200 --text "$ST2 is installed"
                                  fi
            
                              fi
            
# Visual Studio Code
                           if [[ "$instppa" = "$VSC1" ]]
                                  then zenity --question --title="$title" --width=350 --text "This will mount the $VSC1 PPA\nAnd install the App.\nAre you ready?"
                                  if [ "$?" = "0" ]
                                     then (
                                     echo "25"
                                     echo "# Getting Keys From Microsoft"
                                     curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
                                     echo "30"
                                     echo "# Setting Permissions"
                                     mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
 #                                    install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
                                     echo "40"
                                     echo "Mounting PPA"
                                     sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
                                     echo "50"
                                     echo "# Installing transport https"
                                     apt-get install -y apt-transport-https
                                     echo "60"
                                     echo "Updating Repository List"
                                     apt-get update -y
                                     echo "70"
                                     echo ""
                                    apt install -y code
                                    echo "99" ; sleep 1
                                    echo "100"
                                    ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="$VSC1 Installer"
                                    zenity --info --title="$title" --width=200 --text "$VSC1 is installed"
                                  fi
            
                              fi
#curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg && \
#sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg && \
#sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list' && \
#sudo apt-get update -y && \
#sudo apt install -y code code-insiders


# Bluefish Editor
                       if [[ "$instppa" = "$BLF1" ]]
                          then
                            #  zenity --question --title="$title" --width=330 --text="This will mount $BLF1 PPA\nAnd install the Icon Pack.\nAre you ready?"
                             if [ "$?" = "0" ]
                                then (
                                   echo "25"
                                   echo "# Adding Repository"
                                   add-apt-repository -y ppa:klaus-vormweg/bluefish
                                   echo "50"
                                   echo "# Updating Repo List"
                                   apt-get update -y
                                   echo "75"
                                   echo "# Installing $BLF1"
                                   apt-get install -y bluefish 
                                   echo "100"
                                   ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$BLF1 Installer"
                                   zenity --info --title="$title" --width=250 --text "$BLF1 is installed"
                             fi
                       fi
            
# CodeLite Editor
                       if [[ "$instppa" = "$CDL" ]]
                          then
                             zenity --question --title="$title" --width=330 --text="This will mount $CDL PPA\nAnd install the Icon Pack.\nAre you ready?"
                            if [ "$?" = "0" ]
                                then (
                                   echo "25"
                                   echo "# Adding Repository"
                                   add-apt-repository -y ppa:eugenesan/ppa
                                   echo "50"
                                   echo "# Updating Repo List"
                                   apt-get update -y
                                   echo "75"
                                   echo "# Installing $CDL"
                                   apt-get install codelite -y
                                   echo "100"
                                   ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$CDL Installer"
                                   zenity --info --title="$title" --width=250 --text "$CDL is installed"
                            fi
                       fi
            
            
# Atom Editor
                       if [[ "$instppa" = "$ATM1" ]]
                          then
                             zenity --question --title="$title" --width=250 --text "This will mount $ATM1 PPA\nAnd install the $ATM1 App.\nAre you ready?"
                            if [ "$?" = "0" ]
                                then (
                                echo "25"
                                echo "# Adding Repository"
                                sudo add-apt-repository -y ppa:webupd8team/atom
                                echo "50"
                                echo "# Updating Repo List"
                                apt-get update -y
                                echo "75"
                                echo "# Installing Atom"
                                apt-get install -y atom
                                echo "99"
                                echo "All Done." 
                                echo "100"
                                ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="Atom Installer"
                                zenity --info --title="$title" --width=200 --text "$ATM1 is installed"
                           fi
                       fi
            
                       if [[ "$instppa" = "$HTU2" ]]
                          then zenity --info --title="$title" --width=650 --text "This is the IDE and Text Editor window. Here you chose which Code Editor you want to install.\n\nIn here we have put together the best list of Editors that we could find.\nAt least the ones not already in the repos."
                       fi

                       if [[ "$instppa" = "$BACKUP" ]]
                          then break 1
                       fi
            
                       if [[ "$instppa" = "$EXT2" ]]
                          then exit "${?}"
                       fi
                       if [[ "$instppa" = "$BACK_HOME" ]]
                          then break 2
                       fi
                   fi
            done
#------------------------------------------------------------------------------
# Grub Customizer
             if [[ "$pps" = "$GRBCMZR" ]]
                  then
                       zenity --question --title="$title" --width=330 --text="This will mount $GRBCMZR PPA\nAnd install $GRBCMZR.\nAre you ready?"
                      if [ "$?" = "0" ]
                          then (
                             echo "25"
                             echo "# Adding Repository"
                             add-apt-repository -y ppa:danielrichter2007/grub-customizer
                             echo "50"
                             echo "# Updating Repo List"
                             apt-get update -y
                             echo "75"
                             echo "# Installing $GRBCMZR"
                             apt-get install -y grub-customizer
                             echo "99"
                             echo "All Done."                       
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$GRBCMZR Installer"
                             zenity --info --title="$title" --width=250 --text "$GRBCMZR is installed"
                      fi
             fi
      
#------------------------------------------------------------------------------
# Ubuntu Kernel Update Utility
             if [[ "$pps" = "$UKUU1" ]]
                  then
                       zenity --question --title="$title" --width=330 --text="This will mount $UKUU1 PPA\nAnd install the $UKUU1.\nAre you ready?"
                      if [ "$?" = "0" ]
                          then (
                             echo "25"
                             echo "# Adding Repository"
                             sudo add-apt-repository -y ppa:teejee2008/ppa
                             echo "50"
                             echo "# Updating Repo List"
                             apt-get update -y
                             echo "75"
                             echo "# Installing $UKUU1"
                             apt-get install -y ukuu
                             echo "99"
                             echo "All Done." 
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$UKUU1 Installer"
                             zenity --info --title="$title" --width=250 --text "$UKUU1 is installed"
                      fi
             fi
      
# Google Chrome
             if [[ "$pps" = "$CR1" ]]
                  then
                       zenity --question --title="$title" --width=250 --text "This will mount the $CR1 PPA\nAnd install $CR1.\nAre you ready?"
                  if [ "$?" = "0" ]
                      then (
                           echo "15"
                           echo "# Adding Repository"
                           touch /etc/apt/sources.list.d/google-chrome.list
                           echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list
                           echo "30"
                           echo "# Getting Signed Key"
                           wget https://dl.google.com/linux/linux_signing_key.pub
                           echo "40"
                           echo "# Adding Signed Key"
                           apt-key add ~/linux_signing_key.pub
                           echo "50"
                           echo "# Updating Repo List"
                           apt-get update
                           echo "75"
                           echo "# Installing $CR1"
                           apt-get install -y google-chrome-stable
                           echo "100"
                           ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="Chrome Installer"
                         zenity --width=200 --info --text="$CR1 is installed."
                  fi
             fi
      
# Google Earth Pro
             if [[ "$pps" = "$gep1" ]]
                  then
                       zenity --question --title="$title" --width=250 --text "This will mount the $gep1 PPA\nAnd install the App.\nAre you ready?"
                  if [ "$?" = "0" ]
                      then (
                           echo "25"
                           echo "# Adding Repository"
                           wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
                          sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main" > /etc/apt/sources.list.d/google.list'
                          echo "50"
                          echo "# Updating Repo List"
                          apt-get update -y
                          echo "75"
                          echo "# Installing $gep1"
                          apt-get install -y google-earth-pro-stable
                          echo "99" ; sleep 1
                          echo "100"
                          ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="$gep1 Installer"
                          zenity --width=250 --info --text="$gep1 is installed."
                  fi
             fi
      
# Boot Repair
             if [[ "$pps" = "$BRP" ]]
                  then
                       zenity --question --title="$title" --width=330 --text="This will mount $BRP PPA\nAnd install the Icon Pack.\nAre you ready?"
                      if [ "$?" = "0" ]
                          then (
                             echo "25"
                             echo "# Adding Repository"
                             apt-add-repository -y  ppa:yannubuntu/boot-repair
                             echo "50"
                             echo "# Updating Repo List"
                             apt-get update -y
                             echo "75"
                             echo "# Installing $BRP"
                             apt-get install -y boot-repair
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$BRP Installer"
                             zenity --info --title="$title" --width=250 --text "$BRP is installed"
                      fi
             fi
      
# Ubuntu Make
             if [[ "$pps" = "$UM1" ]]
                  then
                       zenity --question --title="$title" --width=330 --text="This will mount $UM1 PPA\nAnd install the $UM1 App.\nAre you ready?"
                      if [ "$?" = "0" ]
                          then (
                             echo "25"
                             echo "# Adding Repository"
                             add-apt-repository -y ppa:ubuntu-desktop/ubuntu-make
                             echo "50"
                             echo "# Updating Repo List"
                             apt-get update -y
                             echo "75"
                             echo "# Installing $UM1"
                             apt-get install -y ubuntu-make
                             echo "100"
                             ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$UM1 Installer"
                             zenity --info --title="$title" --width=250 --text "$UM1 is installed"
                      fi
             fi
      
# Laptop Battery Power
             if [[ "$pps" = "$ltbp1" ]]
                then
                zenity --question --title="$title" --width=250 --text "This will mount the PPA\nAnd install the App.\nSave battery power on laptops\nAre you ready?"
                if [ "$?" = "0" ]
                  then (
                      echo "25"
                      echo "# Adding Repository"
                      add-apt-repository -y ppa:linrunner/tlp
                      echo "50"
                      echo "# Updating Repo List"
                      apt-get update -y
                      echo "75"
                      echo "# Installing TLP"
                      apt-get install -y tlp tlp-rdw
                      tlp start
                      echo "99" ; sleep 1
                      echo "100"
                      ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="TLP Installer"
                      zenity --info --title="$title" --width=200 --text "TLP is installed"
                fi
      
             fi
# laptop-mode-tools
             if [[ "$pps" = "$LMT1" ]]
                then
                zenity --question --title="$title" --width=250 --text "This will mount the PPA\nAnd install the App.\nAre you ready?"
                if [ "$?" = "0" ]
                  then (
                      echo "25"
                      echo "# Adding Repository"
                      sudo add-apt-repository -y ppa:ubuntuhandbook1/apps
                      echo "50"
                      echo "# Updating Repo List"
                      apt-get update -y
                      echo "75"
                      echo "# Installing $LMT1"
                      apt-get install -y laptop-mode-tools
                      tlp start
                      echo "99" ; sleep 1
                      echo "100"
                      ) | zenity --progress --pulsate --auto-close --width=350 --no-cancel --title="$LMT1 Installer"
                      zenity --info --title="$title" --width=200 --text "$LMT1 is installed"
                fi
      
             fi
# kubuntu backports      
             if [[ "$pps" = "$KB1" ]]
                then
                zenity --question --title="$title" --width=270 --text "This will mount the $KB1 PPA\nAnd install the Apps.\nAre you ready?"
                if [ "$?" = "0" ]
                  then (
                      echo "25"
                      echo "# Adding Repository"
                      add-apt-repository -y ppa:kubuntu-ppa/backports
                      echo "50"
                      echo "# Updating Repo List"
                      apt-get update -y
                      echo "75"
                      echo "# Installing kio-gdrive and pkg-kde-tools"
                      apt-get install -y kio-gdrive pkg-kde-tools
                      echo "99" ; sleep 1
                      echo "100"
                      ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$KB1 Installer"
                      zenity --info --title="$title" --width=200 --text "$KB1 are installed"
                fi
      
             fi
# conky-manager (doesn't work in 18.04)
             if [[ "$pps" = "$CM1" ]]
                  then
                    zenity --question --title="$title" --width=250 --text="This Does NOT work in Ubuntu 18.04\nOr Linux Mint 19.\nDo You want to continue?"
                      if [ "$?" = "0" ]
                          then (
                              echo "25"
                              echo "# Adding Repository"
                              apt-add-repository -y ppa:teejee2008/ppa
                              echo "50"
                              echo "# Updating Repo List"
                              apt-get update -y
                              echo "75"
                              echo "# Installing $CM1"
                              apt-get install -y conky-manager
                              echo "99" ; sleep 1
                              echo "100"
                              ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$CM1 Installer"
                              zenity --info --title="$title" --width=250 --text "$CM1 is installed"
                      fi
             fi
# numix-icon-theme-square
             if [[ "$pps" = "$NSI1" ]]
                  then
                       zenity --question --title="$title" --width=250 --text="This will mount $NSI1 PPA\nAnd install the Icon Pack.\nAre you ready?"
                      if [ "$?" = "0" ]
                          then(
                              echo "25"
                              echo "# Adding Repository"
                              add-apt-repository -y ppa:numix/ppa
                              echo "50"
                              echo "# Updating Repo List"
                              apt-get update -y
                              echo "75"
                              echo "# Installing $NSI1 Pack"
                              apt-get install -y numix-icon-theme-square
                              echo "99" ; sleep 1
                              echo "100"
                              ) | zenity --progress --pulsate --auto-close --width=450 --no-cancel --title="$NSI1 Installer"
                              zenity --info --title="$title" --width=250 --text "$NSI1 Pack is installed"
                      fi
             fi
# 
              if [[ "$pps" = "$HTU2" ]]
                 then zenity --info --title="$title" --width=650 --text "This is the PPAs window. Here you chose which PPA you want to install or edit.\n\nMost work with Ubuntu 18.04 and Mint 19, but some do not. be sure to pay attention.\n\nyou can also list all PPAs on your System. Giving you more controle of your computer. "
              fi
      
              if [[ "$pps" = "$EXT2" ]]
                 then exit "${?}"
              fi
      
              if [[ "$pps" = "$BACK_HOME" ]]
                 then break 1
              fi
          fi
      
          done
#------------------------------------------------------------------------------
          while [[ "$home" = "$SFM" ]] ; do
#------------------------------------------------------------------------------
      MES1='Manually Enter Size'
      DS1='Delete Swapfile'
      LFS1='Look At fstab, This is Safe'
      ASF1='Add swapfile to fstab,  Dangerous! '
      
      
      #------------------------------------------------------------------------------
      # swapfile
          if [[ "$home" = "$SFM" ]]
              then swpfl="$(zenity  --list --title="$title" --height=470 --width=300 --cancel-label="Back" --text "${usnm^} To create your swapfile\nChoose The same size as your RAM." --radiolist  --column "Pick" --column "Opinion"  FALSE "$MES1" FALSE "2GB" FALSE "4GB" FALSE "6GB" FALSE "8GB" FALSE "16GB" FALSE "32GB" FALSE "$DS1" FALSE "$LFS1" FALSE "$ASF1" FALSE "$HTU2" FALSE "$EXT2" TRUE "$BACK_HOME" )"
      
              if [ "$swpfl" = "$MES1" ]
                  then NMBR="$( zenity --entry --width=200 --text="How Many GB of RAM?\nUse Numbers ONLY!" --entry-text "6" )"
                       swapoff -a
                       fallocate -l "$NMBR"G /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "2GB" ]
                  then swapoff -a
                       fallocate -l 2048M /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "4GB" ]
                  then swapoff -a
                       fallocate -l 4096M /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "6GB" ]
                  then swapoff -a
                       fallocate -l 6G /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
      
              fi
      
              if [ "$swpfl" = "8GB" ]
                  then swapoff -a
                       fallocate -l 8192M /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "16GB" ]
                  then swapoff -a
                       fallocate -l 16384M /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "32GB" ]
                  then swapoff -a
                       fallocate -l 32768M /swapfile
                       chmod 600 /swapfile
                       mkswap /swapfile
                       swapon -a
              fi
      
              if [ "$swpfl" = "$DS1" ]
                  then swapoff -a
                       rm -f /swapfile
              fi
      
              if [ "$swpfl" = "$LFS1" ]
                 then
                     zenity --text-info --width=600 --height=400 --title="$title" --cancel-label="Back" --filename="/etc/fstab"
              fi
      
              if [[ "$swpfl" = "$ASF1" ]]
                  then zenity --question --title="$title" --width=350 --text="WARNING ! !    If done Wrong\nThis can trash your computer ! ! \n\nAlso,\nIf Gedit is not installed, we will install it now\n\nCOPY this next line\n\n/swapfile none swap defaults 0 0\n\nPASTE it on a line by itself at the VERY BOTTOM\nof the File that opens next\nthen save it and exit the TEXT Editor\n\nThis is a very simple process. BUT!\n\nIF YOU DO NOT FOLLOW THE INSTRUCTIONS ABOVE TO THE LETTER\nYOU WILL TRASH YOUR SYSTEM ! ! Do you wish to continue?"
      
                      if [ "$?" = "0" ]
                         then
                             apt-get install gedit
                              zenity --question --title="$title" --width=350 --text="this next action will make a backup of your fstab.\nThis is very imprtant! It allows you to restore fstab in the event that YOU mess it up.\n\nIf you are not sure what to do, click YES\nIt is better to be safe."
                              if [ "$?" = "0" ]
                                then cp /etc/fstab /etc/fstab.bak
                                zenity --info --width=350 --timeout=3 --text="A backup of fstab has been made at\n/etc/fstab.bak"
                              fi
                             gedit /etc/fstab
                      fi
      
              fi
      
              if [[ "$swpfl" = "$HTU2" ]]
                 then zenity --info --title="$title" --width=650 --text "This is the Swapfile Tool.\n\nHere you can create, Add, Remove or Resize a Swapfile.\n\nYou can also view or edit the fstab.\n\nWARNING ! !\nIf it is done wrong, editing the fstab file can BREAK your system ! !\n\nHowever, if you follow the instructions correctly it will be okay.\n\nA fstab backup (/etc/fstab.bak) can be made\ in case it goes bad.\nSo that you can restore a broken system."
              fi
      
              if [[ "$swpfl" = "$EXT2" ]]
                  then exit "${?}"
              fi
      
              if [[ "$swpfl" = "$BACK_HOME" ]]
                 then break 1
              fi
          fi
          done
#------------------------------------------------------------------------------
          while [[ "$home" = "$CHP1" ]] ; do
      
#------------------------------------------------------------------------------
      USR="Change Password For ${usnm^} "
      RT1='Change Password For ROOT'
      OTR1='Change Other User Password'
      WH0='List ALL USERS on this Computer'
      ANU1='Add a NEW User to the Computer'
      DEL1='DELETE / REMOVE User    (Danger!)'
      CHHN='Change Hostname'
      HowTU3='How to use This Tool'
      
      #------------------------------------------------------------------------------
      # Change passwords
          if [[ "$home" = "$CHP1" ]]
              then chpass="$(zenity --list --title="$title" --height=400 --width=300 --cancel-label="Back" --text="Choose The Tool You Need ${usnm^}" --radiolist --column "Pick" --column "Opinion" FALSE "$RT1" FALSE "$USR" FALSE "$OTR1" FALSE "$GP1" FALSE "$WH0" FALSE "$ANU1" FALSE "$DEL1" FALSE "$CHHN" FALSE "$HowTU3" FALSE "$EXT2" TRUE "$BACK_HOME" )"
      
      # change root password
                 if [[ "$chpass" = "$RT1" ]]
                    then rtpasswd=$(zenity --entry --title="$title" --text "Please enter a root password." --hide-text)
                   rtpasswd2=$(zenity --entry --title="$title" --text "Please re-enter your root password." --hide-text)
                    if [ "$rtpasswd" != "$rtpasswd2" ]
                       then zenity --error --height=500 --width=450 --title="$title" --text "The passwords did not   match, please try again."
                    fi
      
                    echo "# Setting ROOT password..."
                    touch .passwd
                    echo -e "$rtpasswd\n$rtpasswd2" | passwd
                    sudo rm .passwd
                 fi
      
#------------------------------------------------------------------------------
# change user password
                 if [[ "$chpass" = "$USR" ]]
      
                    then userpasswd=$(zenity --entry --title="$title" --text "Please enter a password for ${usnm^}." --hide-text)
                   userpasswd2=$(zenity --entry --title="$title" --text "Please re-enter a password for ${usnm^}." --hide-text)
                      if [ "$userpasswd" != "$userpasswd2" ]
                          then zenity --error --height=500 --width=450 --title="$title" --text "The passwords did not match, please try again."
                      fi
                    echo "# Setting ${usnm^} Password"
                    touch .passwd
                    echo -e "$userpasswd\n$userpasswd2" | passwd $usnm
                    rm .passwd
                 fi
      
#------------------------------------------------------------------------------
# change other user password
                 if [[ "$chpass" = "$OTR1" ]]
      
                    then CHmp1=$(zenity --entry --title="$title" --entry-text "$usnm" --text "Enter The NAME OF THE USER\nWhos Password You Want To Change." )
                    otp1="${CHmp1,,}"
                     grep -q "^${otp1}:" /etc/passwd
                      if [ "$?" = "1" ]
                          then zenity --error --width=250 --title="$title" --timeout=10 --text="Error User \"${otp1^^}\" Does NOT Exist! ! \nZUB will now close ! ! " ;exit "${?}"
                      fi
                  usrpsswd=$(zenity --entry --title="$title" --text "Please enter a password for ${otp1^}." --hide-text)
                  usrpsswd2=$(zenity --entry --title="$title" --text "Please re-enter a password for ${otp1^}." --hide-text)
                      if [ "$usrpsswd" != "$usrpsswd2" ]
                          then zenity --error --height=500 --width=450 --title="$title" --text "The passwords did not match, please try again."
                      fi
      
                    echo "# Setting ${otp1^} Password"
                    touch .passwd
                    echo -e "$usrpsswd\n$usrpsswd2" | passwd $otp1
                    rm .passwd
                 fi
      
#------------------------------------------------------------------------------
# list all users
                 if [[ "$chpass" = "$WH0" ]]
                     then ppa1="$(l=$(grep "^UID_MIN" /etc/login.defs) && awk -F':' -v "limit=${l##UID_MIN}" '{ if ( $3 >= limit ) print $1}' /etc/passwd)"
                     echo "$ppa1" | zenity --text-info --cancel-label="Back" --width=200 --height=300 --title="$title"
                 fi
      
#------------------------------------------------------------------------------
# ADD NEW USER
                 if [[ "$chpass" = "$ANU1" ]]
      
                    then NIPTA=$(zenity --entry --title="$title" --width=250 --text "Please enter a New USER Name\nFor this Computer." )
                    nusr="${NIPTA,,}"
                    grep -q "^${nusr}:" /etc/passwd
                       if [[ "$?" = "0" ]]
                          then zenity --error --width=250 --title="$title" --timeout=10 --text="ERROR User \"${nusr^^}\" Already Exist! ! \nZUB will now close ! ! " ; exit "${?}"
                       fi
                    npasswd=$(zenity --entry --title="$title" --text "Please enter a password for ${nusr^}." --hide-text)
                    npasswd2=$(zenity --entry --title="$title" --text "Please re-enter your password for ${nusr^}." --hide-text)
                    useradd -m -g users -G adm,lp,audio,video -s /bin/bash $nusr
                    if [ "$npasswd" != "$npasswd2" ]
                       then zenity --error --height=500 --width=450 --title="$title" --text "The passwords did not   match, please try again."
                    fi
      
                    echo "# Setting ${nusr^}s password..."
                    touch .passwd
                    echo -e "$npasswd\n$npasswd2" | passwd $nusr
                    sudo rm .passwd
                    zenity --warning --width=200 --text "User ${nusr^} is added."
                 fi
      
#------------------------------------------------------------------------------
# DELETE USER
                  if [[ "$chpass" = "$DEL1" ]]
                      then DUSR1="$(zenity --entry --title="$title" --width=250 --text "Please enter the USER Name\nYou Want To DELETE." )"
      uun2="${DUSR1,,}"
      Da11="DELTE ${uun2^} And their home folder"
      a11="DELETE ${uun2^} But NOT their home folder"
      EXTNO="EXIT Without doing anything"
                       grep -q "^${uun2}:" /etc/passwd
                          if [[ "$?" = "1" ]]
                             then zenity --error --width=250 --title="$title" --timeout=10 --text="ERROR User \"${usnm^^}\" Does Not Exist! ! \nZUB will now close ! ! " ; exit "${?}"
                          fi
      
                       zenity --question  --title="$title" --width=330 --text "Are you Sure You Want To DELETE the USER ${uun2^}?\n\nWARNING You Can NOT Undelete A User."
                      if [[ "$?" = "0" ]]
      
                         then qstn1="$(zenity --list --title="$title" --height=200 --width=320 --cancel-label="Back" --text="Choose The Option You Want" --radiolist --column "Pick" --column "Opinion" FALSE "$a11" FALSE "$Da11" TRUE "$EXTNO" )"
      
                                  if [[ "$qstn1" = "$Da11" ]]
                                       then userdel -r -f $uun2 ; zenity --warning  --title="$title" --width=300 --text "User ${uun2^} And their HOME folder\nhave been deleted!"
                                  fi
      
                                  if [[ "$qstn1" = "$a11" ]]
                                      then userdel $uun2 ; zenity --warning  --title="$title" --width=300 --text "User ${uun2^} has been deleted\nBut their HOME folder was saved."
                                  fi
      
                                  if [[ "$qstn1" = "$EXTNO" ]]
                                     then exit "${?}"
                                  fi
                      fi
                  fi
      
#------------------------------------------------------------------------------
# change Hostname
      hstnm="$(hostname)"
                  if [[ "$chpass" = "$CHHN" ]]
                         then NHNM="$(zenity --entry --width=350 --title="$title" --text "the current Hostname is\n\n${hstnm^^}\n\nWhat do you want to change it to?" --entry-text "$hstnm" )"
                                   if [ "$?" = "1" ] 
                                      then exit  "${?}"
                                      else
                                      echo $NHNM > /etc/hostname
                                      zenity --question --width=300 --text "The New Hostname is $NHNM.\nYou must reboot for changes to take affect.\nDo you want to reboot now?"
                                      if [ "$?" = "0" ]
                                          then reboot
                                      fi
                                  fi
                  fi 
      
#------------------------------------------------------------------------------
# Generate a Password
      NPW1='Randomly Generated Password'
                 if [[ "$chpass" = "$GP1" ]]
                     then
                       NMB="$(zenity --entry --width=250 --title="$title" --text="How Strong? 1-90\nUse Numbers ONLY!" --entry-text "8")"
      #                 HASH="$(date +%s | sha256sum | base64 | head -c "$NMB" )"
                       HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "$NMB")"
                       echo -n $HASH | zenity --text-info --editable --height=10 --width=300 --title="The New $NPW1 Is"
                 fi
      
#------------------------------------------------------------------------------
# HOW TO USE del user
                  if [[ "$chpass" = "$HowTU3" ]]
                    then 
                          zenity --info --title="$CHP1 Help Page" --width=500 --text="Here You can manage, Add, And remove Users.\nThough I Warn You. Once you Delete a USER, it is Gone for Good. \nThere is NO Un-Delete. Use Extreme Caution ! \n\nYou can change the Root password, or a Users password. This is useful \n\nYou can get a list of all users registered on this computer. \n\nYou can Also change the Hostname. \nAfter making changes, it is good practice to reboot the system. \nOr at least log out and back in. "
                  fi

#------------------------------------------------------------------------------
                 if [[ "$chpass" = "$EXT2" ]]
                   then exit "${?}"
                 fi
      
                 if [[ "$chpass" = "$BACK_HOME" ]]
                   then break 1
                 fi
          fi
         done
#------------------------------------------------------------------------------

      LN1='LOTTO NUMBERS'
#------------------------------------------------------------------------------
# Lotto Fun
         if  [[ "$home" = "$LF1" ]]
                  then NMBR="$( zenity --entry --width=250 --cancel-label="Ten Numbers" --title="$LN1" --text="How Many Numbers?" --entry-text "6" )"
                       LTO="$( seq -w 99 | sort -R | head -"$NMBR" | fmt | tr " " "-" )"
                       RNDM="Your $LN1 Are"
                       echo -n $LTO | zenity --text-info --editable --height=10 --width=300 --title="$RNDM" --width=300
          fi
      
          if [[ "$home" = "$HTU" ]]
              then zenity --info --title="$title" --width=650 --height=300 --text "This is the HOME window of the ZUB Toolbox. Here you chose which ZUB Tool you want to use.\n\n$UPO1:   To update your Opperating System and other things.\nThis is the most used Tool.\n\n$IAD:    As the title implies, this is where you add another DeskTop Environment.\n\n$PPA1:   To mount the PPA and install the App(s) associated with it.\n\n$SFM:   To create, delete, and or ajust Swapfies.\n\n$GP1:   Just like the title says, it Generates a random password. A very useful Tool.\n\n$LF1:    Is a tool  I made for my own amusement. It will generate random lottory numbers.\nJust enter the amount of numbers you want.\n\n$EXT1:   Choose between exiting this App, rebooting, or turning off the Computer."
          fi
      
#------------------------------------------------------------------------------
      PA1='Power Adminastration'
      RB1='Reboot This Computer'
      SD1='Shut Down This Computer'
      
#------------------------------------------------------------------------------
# EXIT Options
          if [[ "$home" = "$EXT1" ]]
              then PWR="$(zenity  --list --title="$title" --cancel-label="Back" --height=200 --width=250 --text "$PA1" --radiolist  --column "Pick" --column "Opinion"  FALSE "$RB1" FALSE "$SD1" TRUE "$EXT3" )"
      
              if [[ "$PWR" = "$EXT3" ]]
                 then exit "${?}"
              fi
      
              if [[ "$PWR" = "$RB1" ]]
                then reboot
              fi
      
              if [[ "$PWR" = "$SD1" ]]
                 then shutdown -h now
              fi
          fi
      
done
#------------------------------------------------------------------------------
# END OF SCRIPT