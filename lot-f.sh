#!/bin/bash

# Created by ELIAS WALKER
#-------------------------------------------------------
# This code was writen 12/Jan/2016
# The lot-f.sh script was Created on 31/Jan/2020
# Undergoing development to date.
#-------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
########################################################################
####################### Acknowledgements ###############################
########################################################################
#
#
#
########################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       
#       paypal.me/eliwal
#######################################################################
####################### BEGINNING OF SCRIPT ###########################
#######################################################################

Lotto_fun() {
            clear ; clear
            mkfifo /tmp/namedPipe5 # this creates named pipe, aka fifo
            # to make sure the shell doesn't hang, we run redirection
            # in background, because fifo waits for output to come out
            dialog --title "${DWS1}" --cancel-label "Ten Numbers" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "8" 2> /tmp/namedPipe5 &
            # release contents of pipe
            LTO2="$( cat /tmp/namedPipe5  )"
            clear
            clear
            LTO="$( seq -w 99 | sort -R | head -"${LTO2}" | fmt | tr " " "-" )"
            echo ""
            echo "The Randomly Generated Lotto Number Is"
            echo ""
            echo "$LTO"
            echo ""
            echo "Press a key to continue"
            read -rsn1
            echo "Key pressed" ; sleep 1
            dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Lotto Number Is \n\n$LTO " 10 45
            # clean up
            rm /tmp/namedPipe5
            echo ""
}
## End of Lotto_fun Function


Lotto_fun
clear ; clear
